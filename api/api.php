<?php

	require_once("Rest.inc.php");
	require_once("db.php");
	require_once("functions.php");

	class API extends REST {

		private $functions = NULL;
		private $db = NULL;

		public function __construct() {
			$this->db = new DB();
			$this->functions = new functions($this->db);
		}

		public function check_connection() {
			$this->functions->checkConnection();
		}

		/*
		 * ALL API Related android client -------------------------------------------------------------------------
		*/

		private function get_news_detail() { // added news_gallery
	        $this->functions->getNewsById();
	    }

	    private function get_post_detail() { //added news_gallery
	        $this->functions->getNewsDetail();
	    }
		
		private function get_category_index() { //added news_gallery
	        $this->functions->getCategoryIndex();
	    }

        private function get_urgent_posts() { //added news_gallery
            $this->functions->getUrgentPosts();
        }
		private function get_category_posts() { //added news_gallery
	        $this->functions->getCategoryPosts();
		}
		private function get_all_categories(){
			$this->functions->getAllCategories();
		}

	    private function get_search_results() { //added news_gallery
	        $this->functions->getSearchResults();
	    }
	    private function get_recent_posts() { //added news_gallery
			$this->functions->getRecentPosts();
		}
	    private function get_favorite_posts() { //added news_gallery
			$this->functions->getFavorites();
		}
		private function add_favorite() { //added news_gallery
			$this->functions->addToFavorites();
		}
		private function remove_favorite() { //added news_gallery
			$this->functions->removeFromFavorites();
		}
		// ** ** ** ** ** *** ** ** ** * * * ** * *//

		private function get_video_posts() {
			$this->functions->getVideoPosts();
		}

		
		//new code added by mirette for register from website angular site 9-12-2019
		private function user_register_website() {
	        $this->functions->userRegisterWebsite();
		}
		//end added code
		//new code added by mirette 15-9-2019 for login api for websote
		private function user_login_website() {
	        $this->functions->userLoginWebsite();
		}
		//end added code

		//added code by mirette 15-9-2019 edit user by website
		private function update_user_website() {
	        $this->functions->updateUserWebsite();
		}
		//end of added code by mirette
	    private function user_register() {
	        $this->functions->userRegister();
	    }

	    private function get_user() {
	        $this->functions->getUser();
	    }

	    private function get_user_login() {
	        $this->functions->getUserLogin();
	    }

	    private function get_user_profile() {
	        $this->functions->getUserProfile();
		}
		private function get_search_post() {
	        $this->functions->getSearchPost();
	    }

	    private function update_user_profile() {
	        $this->functions->updateUserProfile();
	    }
		private function update_user_photo() {
	        $this->functions->updateUserPhoto();
	    }
	    
		//added new api by mirette for upload photo from website
		private function update_user_photoweb() {
	        $this->functions->updateUserPhotoWeb();
	    }
		
		//end
	    private function get_comments() {
	        $this->functions->getComments();
	    }

	    private function post_comment() {
	        $this->functions->postComment();
	    } 

	    private function update_comment() {
	        $this->functions->updateComment();
	    }

	    private function delete_comment() {
	        $this->functions->deleteComment();
	    }

	    private function forgot_password() {
	        $this->functions->forgotPassword();
	    }

	    private function get_privacy_policy() {
	        $this->functions->getPrivacyPolicy();
	    }

		/*
		 * End of API Transactions ----------------------------------------------------------------------------------
		*/

		public function processApi() {
			if(isset($_REQUEST['x']) && $_REQUEST['x']!=""){
				$func = strtolower(trim(str_replace("/","", $_REQUEST['x'])));
				if((int)method_exists($this,$func) > 0) {
					$this->$func();
				} else {
					echo 'processApi - method not exist';
					exit;
				}
			} else {
				echo 'processApi - method not exist';
				exit;
			}
		}

	}

	// Initiiate Library
	$api = new API;
	$api->processApi();

?>
