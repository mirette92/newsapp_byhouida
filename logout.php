<?php
	include('session-init.php');
	unset($_SESSION['user']);
	unset($_SESSION['timeout']);
	session_destroy();
	header("location:index.php");
?>