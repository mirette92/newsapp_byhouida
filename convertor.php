<?php include 'includes/config.php' ?>
<?php include_once('public/functions.php'); ?>
<?php
$config=$GLOBALS['config'];
$query="SELECT * FROM tbl_category";
$result=mysqli_query($config,$query);
while($row=mysqli_fetch_assoc($result)){
    functions::InsertCategoryWP(['name'=>$row['category_name']],$row['cid']);
}
$query="SELECT * FROM tbl_news";
$result=mysqli_query($config,$query);
while($row=mysqli_fetch_assoc($result)){
    $cid=$row['cat_id'];
    $query="SELECT * FROM tbl_category WHERE cid='$cid'";
    $cresult=mysqli_query($config,$query);
    $crow=mysqli_fetch_assoc($cresult);
    if($crow['wp_id']==null||$crow['wp_id']){
        $crow['wp_id']=functions::InsertCategoryWP(['name'=>$crow['category_name']],$crow['cid']);
    }
    $newsParameters=[ 'date'=>date('Y-m-d H:i:s',strtotime($row['news_date'])),
        'date_gmt'=>gmdate('Y-m-d H:i:s',strtotime($row['news_date'])),
        'status'=>'publish',
        'title'=>$row['news_title'],
        'content'=>$row['news_description'],
        'categories'=>$cresult['wp_id']];
    functions::InsertNewsWP($newsParameters,null,null,$row['nid'],$row['wp_id']);
}
?>