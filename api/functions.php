<?php 
require_once("Rest.inc.php");
require_once("db.php");

class functions extends REST {
    
    private $mysqli = NULL;
    private $db = NULL;
    
    public function __construct($db) {
        parent::__construct();
        $this->db = $db;
        $this->mysqli = $db->mysqli;
    }

	public function checkConnection() {
			if (mysqli_ping($this->mysqli)) {
                $respon = array(
                    'status' => 'ok', 'database' => 'connected'
                );
                $this->response($this->json($respon), 200);
			} else {
                $respon = array(
                    'status' => 'failed', 'database' => 'not connected'
                );
                $this->response($this->json($respon), 404);
			}
	}

	

    public function getUrgentPosts() {

        include "../includes/config.php";
        $setting_qry    = "SELECT * FROM tbl_settings where id = '1'";
        $setting_result = mysqli_query($connect, $setting_qry);
        $settings_row   = mysqli_fetch_assoc($setting_result);
        $api_key    = $settings_row['api_key'];

        if (isset($_GET['api_key'])) {

            $access_key_received = $_GET['api_key'];

            if ($access_key_received == $api_key) {

                if($this->get_request_method() != "GET") $this->response('',406);
                $limit = isset($this->_request['count']) ? ((int)$this->_request['count']) : 10;
				$page = isset($this->_request['page']) ? ((int)$this->_request['page']) : 1;
				
				 //old code
                // $user_id = isset($this->_request['user_id']) ? ((int)$this->_request['user_id']) : 0;
                // $user_id = isset($this->_request['user_id']) ? ((int)$this->_request['user_id']) : 0;
    //end old code
    //mew code added by mirette 3-10
    $user_id = isset($_GET['user_id']) ? ((int)$_GET['user_id']) : 0;
    //end new code

                $offset = ($page * $limit) - $limit;
                $count_total = $this->get_count_result("SELECT COUNT(DISTINCT n.nid) FROM tbl_news n WHERE n.content_type = 'Post' AND n.urgent='1' ");
                $query = "SELECT DISTINCT n.nid, 
									n.news_title, 
									n.cat_id,
									n.news_date, 
									n.news_image, 
									n.news_description,
									n.video_url,
									n.video_id, 
									n.content_type, 
									
									c.category_name, 
									COUNT(DISTINCT r.comment_id) as comments_count

								  FROM tbl_news n 

								  LEFT JOIN tbl_comments r ON n.nid = r.nid 
								  LEFT JOIN tbl_category c ON n.cat_id = c.cid

								  WHERE n.content_type = 'Post' AND n.urgent='1'

								  GROUP BY n.nid 

								  ORDER BY n.nid 

								  DESC LIMIT $limit OFFSET $offset";

                $categories = $this->get_list_result($query);
                foreach ($categories as $key => $value) {
                	// show favorite flag

              
				 	$date = strtotime($categories[$key]['news_date']);
				 	// $new_date = date(" H:i:s", $categories[$key]['news_date']);
                                       
				// 	$categories[$key]['news_date'] = $new_date;

                    //  print($new_date);die();
                	
                	if ($user_id != 0)
                	{

                		  $sql= "select count(*) as count from tbl_favorites where user_id=$user_id and article_id = ".$value['nid'] ;
                			$cnt = mysqli_query($connect,$sql);  
                			$cnt= mysqli_fetch_assoc($cnt);
                		if ($cnt['count'])
                			$categories[$key]['favorite'] = 1;
                		else
                			$categories[$key]['favorite'] = 0;

                	} else { $categories[$key]['favorite'] = 0; }
							if ( is_array(json_decode($value['news_image'])))
							{
								$images = json_decode($value['news_image']);
								$image = $images[0];
								unset($images[0]);
								$gallery_object = count($images) ? $images:[];
								$gallery = []; 
								$categories[$key]['news_image'] = $image;
								//code add by mirette in 9-12-2019
								$categories[$key]['news_image'] = $image;
								//end of code added by mirette
								foreach ($gallery_object as $k => $img) {
									$gallery[] = $img;
								}
								$categories[$key]['news_gallery'] = $gallery;
							}else
							{
								 	//code add by mirette in 9-12-2019
									$categories[$key]['news_image'] = $value['news_image'];
									//end of code added by mirette
									$categories[$key]['news_gallery'] = [];
								 
							}
						}
				$count = count($categories);
				
                $respon = array(
                    'status' => 'ok', 'count' => $count, 'count_total' => $count_total, 'pages' => $page, 'posts' => $categories
                );
                $this->response($this->json($respon,'UTF-8'), 200);

            } else {
                $respon = array( 'status' => 'failed', 'message' => 'Oops, API Key is Incorrect!');
                $this->response($this->json($respon), 404);
            }
        } else {
            $respon = array( 'status' => 'failed', 'message' => 'Forbidden, API Key is Required!');
            $this->response($this->json($respon), 404);
        }

	}
	public function getSearchPost() {

        include "../includes/config.php";
        $setting_qry    = "SELECT * FROM tbl_settings where id = '1'";
        $setting_result = mysqli_query($connect, $setting_qry);
        $settings_row   = mysqli_fetch_assoc($setting_result);
        $api_key    = $settings_row['api_key'];

        if (isset($_GET['api_key'])) {
			$searchKey = $_GET['search'];
			// print($searchKey);die;
            $access_key_received = $_GET['api_key'];

            if ($access_key_received == $api_key) {

                if($this->get_request_method() != "GET") $this->response('',406);
                $limit = isset($this->_request['count']) ? ((int)$this->_request['count']) : 10;
				$page = isset($this->_request['page']) ? ((int)$this->_request['page']) : 1;
				
				 //old code
                // $user_id = isset($this->_request['user_id']) ? ((int)$this->_request['user_id']) : 0;
                // $user_id = isset($this->_request['user_id']) ? ((int)$this->_request['user_id']) : 0;
				//end old code
				//mew code added by mirette 3-10
				$user_id = isset($_GET['user_id']) ? ((int)$_GET['user_id']) : 0;
				//end new code

                $offset = ($page * $limit) - $limit;
                $count_total = $this->get_count_result("SELECT COUNT(DISTINCT n.nid) FROM tbl_news n WHERE n.content_type = 'Post' AND n.urgent='1' ");
                $query = "SELECT DISTINCT n.nid, 
									n.news_title, 
									n.cat_id,
									n.news_date, 
									n.news_image, 
									n.news_description,
									n.video_url,
									n.video_id, 
									n.content_type, 
									
									c.category_name, 
									COUNT(DISTINCT r.comment_id) as comments_count

								  FROM tbl_news n 

								  LEFT JOIN tbl_comments r ON n.nid = r.nid 
								  LEFT JOIN tbl_category c ON n.cat_id = c.cid

								  WHERE n.content_type = 'Post' AND n.urgent='1'
								  AND n.news_title LIKE '%$searchKey%'
								  GROUP BY n.nid 

								  ORDER BY n.nid 

								  DESC LIMIT $limit OFFSET $offset";
				// print($query);die();
                $categories = $this->get_list_result($query);
                foreach ($categories as $key => $value) {
                	// show favorite flag

              
				 	$date = strtotime($categories[$key]['news_date']);
				 	// $new_date = date(" H:i:s", $categories[$key]['news_date']);
                                       
				// 	$categories[$key]['news_date'] = $new_date;

                    //  print($new_date);die();
                	
                	if ($user_id != 0)
                	{

                		  $sql= "select count(*) as count from tbl_favorites where user_id=$user_id and article_id = ".$value['nid'] ;
                			$cnt = mysqli_query($connect,$sql);  
                			$cnt= mysqli_fetch_assoc($cnt);
                		if ($cnt['count'])
                			$categories[$key]['favorite'] = 1;
                		else
                			$categories[$key]['favorite'] = 0;

                	} else { $categories[$key]['favorite'] = 0; }
							if ( is_array(json_decode($value['news_image'])))
							{
								$images = json_decode($value['news_image']);
								$image = $images[0];
								unset($images[0]);
								$gallery_object = count($images) ? $images:[];
								$gallery = []; 
								$categories[$key]['news_image'] = $image;
								//code add by mirette in 9-12-2019
								$categories[$key]['news_image'] = $image;
								//end of code added by mirette
								foreach ($gallery_object as $k => $img) {
									$gallery[] = $img;
								}
								$categories[$key]['news_gallery'] = $gallery;
							}else
							{
								 	//code add by mirette in 9-12-2019
									$categories[$key]['news_image'] = $value['news_image'];
									//end of code added by mirette
									$categories[$key]['news_gallery'] = [];
								 
							}
						}
				$count = count($categories);
				
                $respon = array(
                    'status' => 'ok', 'count' => $count, 'count_total' => $count_total, 'pages' => $page, 'posts' => $categories
                );
                $this->response($this->json($respon,'UTF-8'), 200);

            } else {
                $respon = array( 'status' => 'failed', 'message' => 'Oops, API Key is Incorrect!');
                $this->response($this->json($respon), 404);
            }
        } else {
            $respon = array( 'status' => 'failed', 'message' => 'Forbidden, API Key is Required!');
            $this->response($this->json($respon), 404);
        }

    }

   	public function addToFavorites() {
   		
		include "../includes/config.php";
		    $setting_qry    = "SELECT * FROM tbl_settings where id = '1'";
		    $setting_result = mysqli_query($connect, $setting_qry);
		    $settings_row   = mysqli_fetch_assoc($setting_result);
		    $api_key    = $settings_row['api_key'];
		    if (!isset($_GET['api_key']) || $api_key != @$_GET['api_key'] )
			{
 				$respon = array( 'status' => 'failed', 'message' => 'Forbidden, API Key is Required!');
				$this->response($this->json($respon), 404);
			}
		$user_id = $_GET['user_id'];
		$article_id = $_GET['article_id'];
			header( 'Content-Type: application/json; charset=utf-8' );
		$sql = "Select * FROM tbl_favorites where user_id = $user_id and article_id= $article_id order by id desc";
		$rows = mysqli_query($connect, $sql);
		if (!mysqli_num_rows($rows))
		{
				$sql = "insert into tbl_favorites(user_id,article_id) values('$user_id','$article_id')";
				$insert = mysqli_query($connect,
					$sql);
			if ($insert)
			{
				$result = [ 'success'=>'ok', 'message'=>'successfully added to favorites'];
			header( 'Content-Type: application/json; charset=utf-8' );

				echo json_encode($result);
			return;
			}
		}
		else {
			$result = [ 'success'=>'failed', 'message'=>'already in favorites'];

				echo json_encode($result);
			return;
		}
		$result = [ 'success'=>'failed', 'message'=>'Someting went wrong'];
		echo json_encode($result);
		return;

   	}
   	public function removeFromFavorites() {
   		
		include "../includes/config.php";
		    $setting_qry    = "SELECT * FROM tbl_settings where id = '1'";
		    $setting_result = mysqli_query($connect, $setting_qry);
		    $settings_row   = mysqli_fetch_assoc($setting_result);
		    $api_key    = $settings_row['api_key'];
		    if (!isset($_GET['api_key']) || $api_key != @$_GET['api_key'] )
			{
 				$respon = array( 'status' => 'failed', 'message' => 'Forbidden, API Key is Required!');
				$this->response($this->json($respon), 404);
			}
		$user_id = $_GET['user_id'];
		$article_id = $_GET['article_id'];
			header( 'Content-Type: application/json; charset=utf-8' );
		$sql = "Select * FROM tbl_favorites where user_id = $user_id and article_id= $article_id order by id desc";
		$rows = mysqli_query($connect, $sql);
		if (mysqli_num_rows($rows))
		{
				$sql = "DELETE FROM tbl_favorites WHERE user_id=$user_id and article_id=$article_id";
				$deleted = mysqli_query($connect,
					$sql);
			if ($deleted)
			{
				$result = [ 'success'=>'ok', 'message'=>'successfully removed from favorites'];
			header( 'Content-Type: application/json; charset=utf-8' );

				echo json_encode($result);
			return;
			}
		}
		else {
			$result = [ 'success'=>'failed', 'message'=>'Article is not in favorite '];

				echo json_encode($result);
			return;
		}
		$result = [ 'success'=>'failed', 'message'=>'Someting went wrong'];
		echo json_encode($result);
		return;

   	}

    public function getRecentPosts() {

    		include "../includes/config.php";
		    $setting_qry    = "SELECT * FROM tbl_settings where id = '1'";
		    $setting_result = mysqli_query($connect, $setting_qry);
		    $settings_row   = mysqli_fetch_assoc($setting_result);
		    $api_key    = $settings_row['api_key'];
			// print_r($_GET['user_id']);die();
			if (isset($_GET['api_key'])) {

				$access_key_received = $_GET['api_key'];

				if ($access_key_received == $api_key) {

					if($this->get_request_method() != "GET") $this->response('',406);
						$limit = isset($this->_request['count']) ? ((int)$this->_request['count']) : 10;
						$page = isset($this->_request['page']) ? ((int)$this->_request['page']) : 1;
						 //old code
                // $user_id = isset($this->_request['user_id']) ? ((int)$this->_request['user_id']) : 0;
                // $user_id = isset($this->_request['user_id']) ? ((int)$this->_request['user_id']) : 0;
    //end old code
    //mew code added by mirette 3-10
    $user_id = isset($_GET['user_id']) ? ((int)$_GET['user_id']) : 0;
    //end new code
						
						$offset = ($page * $limit) - $limit;
						$last_id = isset($_GET['last_id']) ? $_GET['last_id']:99999999999;
						$count_total = $this->get_count_result("SELECT COUNT(DISTINCT n.nid) FROM tbl_news n WHERE n.content_type = 'Post' and n.nid < $last_id");
						$query = "SELECT DISTINCT n.nid, 
									n.news_title, 
									n.cat_id,
									n.news_date, 
									n.news_image, 
									 n.news_description ,
									n.video_url,
									n.video_id, 
									n.content_type, 
									
									c.category_name, 
									COUNT(DISTINCT r.comment_id) as comments_count

								  FROM tbl_news n 

								  LEFT JOIN tbl_comments r ON n.nid = r.nid 
								  LEFT JOIN tbl_category c ON n.cat_id = c.cid

								  WHERE n.nid < $last_id  AND n.content_type = 'Post' 

								  GROUP BY n.nid 

								  ORDER BY n.nid 

								  DESC LIMIT $limit OFFSET $offset";

						$categories = $this->get_list_result($query);
						foreach ($categories as $key => $value) {
							
							
							// show favorite flag
                                // print($categories[key]['news_date']);die();
			                	if ($user_id != 0)
			                	{

			                		  $sql= "select count(*) as count from tbl_favorites where user_id=$user_id and article_id = ".$value['nid'] ;
			                			$cnt = mysqli_query($connect,$sql);  
			                			$cnt= mysqli_fetch_assoc($cnt);
			                		if ($cnt['count'])
			                			$categories[$key]['favorite'] = 1;
			                		else
			                			$categories[$key]['favorite'] = 0;

			                	}
			                	else { $categories[$key]['favorite'] = 0; }

							if ( is_array(json_decode($value['news_image'])))
							{
								$images = json_decode($value['news_image']);
								$image = $images[0];
								unset($images[0]);
								$gallery_object = count($images) ? $images:[];
								$gallery = []; 
								//code add by mirette in 9-12-2019
								$categories[$key]['news_image'] = $image;
								//end of code added by mirette
								foreach ($gallery_object as $k => $img) {
									$gallery[] = $img;
								}
								$categories[$key]['news_gallery'] = $gallery;

							} 
								else
							{
								 //code add by mirette in 9-12-2019
									$categories[$key]['news_image'] = $value['news_image'];
									//end of code added by mirette
									$categories[$key]['news_gallery'] = [];
								 
							}
						}
						$count = count($categories);

						$respon = array(
							  'status' => 'ok', 'count' => $count, 'count_total' => $count_total, 'pages' => $page, 'posts' => $categories
						);
						$this->response($this->json($respon), 200);

				} else {
					$respon = array( 'status' => 'failed', 'message' => 'Oops, API Key is Incorrect!');
					$this->response($this->json($respon), 404);
				}
			} else {
				$respon = array( 'status' => 'failed', 'message' => 'Forbidden, API Key is Required!');
				$this->response($this->json($respon), 404);
			}

    }
   
  	public function getFavorites() {

    		include "../includes/config.php";
		    $setting_qry    = "SELECT * FROM tbl_settings where id = '1'";
		    $setting_result = mysqli_query($connect, $setting_qry);
		    $settings_row   = mysqli_fetch_assoc($setting_result);
		    $api_key    = $settings_row['api_key'];

			if (isset($_GET['api_key'])) {

				$access_key_received = $_GET['api_key'];

				if ($access_key_received == $api_key) {
					$user_id = $_GET['user_id'];
					// print($user_id);die();
					$sql = "select article_id from tbl_favorites where user_id=$user_id order by id desc";
					$get_favorites = mysqli_query($connect, $sql );
					 $get_favorites= mysqli_fetch_assoc($get_favorites);
					  
					if($this->get_request_method() != "GET") $this->response('',406);
						 
						    $query = "SELECT DISTINCT n.nid, 
									n.news_title, 
									n.cat_id,
									n.news_date, 
									n.news_image, 
									 n.news_description ,
									n.video_url,
									n.video_id, 
									n.content_type, 
									
									c.category_name, 
									COUNT(DISTINCT r.comment_id) as comments_count

								  FROM tbl_news n 

								  LEFT JOIN tbl_comments r ON n.nid = r.nid 
								  LEFT JOIN tbl_category c ON n.cat_id = c.cid
								  LEFT JOIN tbl_favorites f ON n.nid = f.article_id
								  WHERE n.content_type = 'Post' 

								  and  n.nid in (select article_id from tbl_favorites where user_id=$user_id order by tbl_favorites.id desc)
								  GROUP BY n.nid
								  ORDER BY f.id desc
									";   
						$categories = $this->get_list_result($query);
						foreach ($categories as $key => $value) {
							$categories[$key]['favorite'] = 1;
							if ( is_array(json_decode($value['news_image'])))
							{
								$images = json_decode($value['news_image']);
								$image = $images[0];
								unset($images[0]);
								$gallery_object = count($images) ? $images:[];
								$gallery = []; 
								$categories[$key]['news_image'] = $image;
								foreach ($gallery_object as $k => $img) {
									$gallery[] = $img;
								}
								$categories[$key]['news_gallery'] = $gallery;
							}else
							{
								 
									$categories[$key]['news_image'] = $value['news_image'];
									$categories[$key]['news_gallery'] = [];
								 
							}
						}
						$count = count($categories);

						$respon = array(
							  'status' => 'ok' ,'posts' => $categories
						);
						$this->response($this->json($respon), 200);

				} else {
					$respon = array( 'status' => 'failed', 'message' => 'Oops, API Key is Incorrect!');
					$this->response($this->json($respon), 404);
				}
			} else {
				$respon = array( 'status' => 'failed', 'message' => 'Forbidden, API Key is Required!');
				$this->response($this->json($respon), 404);
			}

    }

	public function getNewsById() {

		include "../includes/config.php";

		$id = $_GET['id'];
		
		$sql = "SELECT DISTINCT n.nid, 
						n.news_title, 
						n.cat_id,
						n.news_date, 
						n.news_image, 
						n.news_description,
						n.video_url,
						n.video_id, 
						n.content_type, 
									
						c.category_name, 
						COUNT(DISTINCT r.comment_id) as comments_count

						FROM tbl_news n 

						LEFT JOIN tbl_comments r ON n.nid = r.nid 
						LEFT JOIN tbl_category c ON n.cat_id = c.cid 

						WHERE n.nid = $id

						GROUP BY n.nid
								 
						LIMIT 1";
		$result = mysqli_query($connect, $sql);
		$result = mysqli_fetch_assoc($result);

							if ( is_array(json_decode($result['news_image'])))
							{
								$images = json_decode($result['news_image']);
								$image = $images[0];
								unset($images[0]);
								$gallery_object = count($images) ? $images:[];
								$gallery = []; 
								$result['news_image'] = $image;
								foreach ($gallery_object as $k => $img) {
									$gallery[] = $img;
								}
								$result['news_gallery'] = $gallery;
							}else
							{
								 
									$result['news_image'] = $value['news_image'];
									$result['news_gallery'] = [];
								 
							}
						 
		header( 'Content-Type: application/json; charset=utf-8' );
		print json_encode( $result );


	}    

	public function getNewsDetail() {

    	$id = $_GET['id'];

		if($this->get_request_method() != "GET") $this->response('',406);

		$query_post = "SELECT DISTINCT n.nid, 
						n.news_title, 
						n.cat_id,
						n.news_date, 
						n.news_image, 
						n.news_description,
						n.video_url,
						n.video_id, 
						n.content_type, 
									
						c.category_name, 
						COUNT(DISTINCT r.comment_id) as comments_count

						FROM tbl_news n 

						LEFT JOIN tbl_comments r ON n.nid = r.nid 
						LEFT JOIN tbl_category c ON n.cat_id = c.cid 

						WHERE n.nid = $id

						GROUP BY n.nid
								 
						LIMIT 1";

		$post = $this->get_one($query_post);
		$count = count($post); 
		$respon = array(
			'status' => 'ok', 'post' => $post
		);  
		$respon['post']['news_description'] = str_replace (   '\\','',$respon['post']['news_description']);
		$respon['post']['news_description'] = htmlspecialchars_decode(str_replace (   'alt=""','',$respon['post']['news_description']));
		

		if ( is_array(json_decode($respon['post']['news_image'])))
		{
								$images = json_decode($respon['post']['news_image']);
								$image = $images[0];
								unset($images[0]);
								$gallery_object = count($images) ? $images:[];
								$gallery = []; 
								$respon['post']['news_image'] = $image;
								foreach ($gallery_object as $k => $img) {
									$gallery[] = $img;
								}
								$respon['post']['news_gallery'] = $gallery;
							}else
							{
								 
									// $respon['post']['news_image'] = $value['news_image'];
									$respon['post']['news_gallery'] = [];
								 
		} 

		$re = $this->json($respon);
		echo $this->response($re,200) ;
    }

    public function getVideoPosts() {

    		include "../includes/config.php";
		    $setting_qry    = "SELECT * FROM tbl_settings where id = '1'";
		    $setting_result = mysqli_query($connect, $setting_qry);
		    $settings_row   = mysqli_fetch_assoc($setting_result);
		    $api_key    = $settings_row['api_key'];

			if (isset($_GET['api_key'])) {

				$access_key_received = $_GET['api_key'];

				if ($access_key_received == $api_key) {

					if($this->get_request_method() != "GET") $this->response('',406);
						$limit = isset($this->_request['count']) ? ((int)$this->_request['count']) : 10;
						$page = isset($this->_request['page']) ? ((int)$this->_request['page']) : 1;
						
						$offset = ($page * $limit) - $limit;
						$count_total = $this->get_count_result("SELECT COUNT(DISTINCT n.nid) FROM tbl_news n WHERE n.content_type != 'Post' ");
						$query = "SELECT DISTINCT n.nid, 
									n.news_title, 
									n.cat_id,
									n.news_date, 
									n.news_image, 
									n.news_description,
									n.video_url,
									n.video_id, 
									n.content_type, 
									
									c.category_name, 
									COUNT(DISTINCT r.comment_id) as comments_count

								  FROM tbl_news n 

								  LEFT JOIN tbl_comments r ON n.nid = r.nid 
								  LEFT JOIN tbl_category c ON n.cat_id = c.cid

								  WHERE n.content_type != 'Post'

								  GROUP BY n.nid 

								  ORDER BY n.nid 

								  DESC LIMIT $limit OFFSET $offset";

						$categories = $this->get_list_result($query);
						$count = count($categories);
						$respon = array(
							'status' => 'ok', 'count' => $count, 'count_total' => $count_total, 'pages' => $page, 'posts' => $categories
						);
						$this->response($this->json($respon), 200);

				} else {
					$respon = array( 'status' => 'failed', 'message' => 'Oops, API Key is Incorrect!');
					$this->response($this->json($respon), 404);
				}
			} else {
				$respon = array( 'status' => 'failed', 'message' => 'Forbidden, API Key is Required!');
				$this->response($this->json($respon), 404);
			}

    }
    
    public function getCategoryIndex() {

    	include "../includes/config.php";
        $setting_qry    = "SELECT * FROM tbl_settings where id = '1'";
		$setting_result = mysqli_query($connect, $setting_qry);
		$settings_row   = mysqli_fetch_assoc($setting_result);
		$api_key    = $settings_row['api_key'];

			if (isset($_GET['api_key'])) {

				$access_key_received = $_GET['api_key'];

				if ($access_key_received == $api_key) {

					if($this->get_request_method() != "GET") $this->response('',406);
					$count_total = $this->get_count_result("SELECT COUNT(DISTINCT cid) FROM tbl_category");

					//$query = "SELECT distinct cid, category_name, category_image FROM tbl_category ORDER BY cid DESC";
					$query = "SELECT DISTINCT c.cid, c.category_name, c.category_image, COUNT(DISTINCT r.nid) as post_count
					  FROM tbl_category c LEFT JOIN tbl_news r ON c.cid = r.cat_id GROUP BY c.cid ORDER BY c.category_order DESC";

					$news = $this->get_list_result($query);
					$count = count($news);
					$respon = array(
						'status' => 'ok', 'count' => $count, 'categories' => $news
					);
					$this->response($this->json($respon), 200);

				} else {
					$respon = array( 'status' => 'failed', 'message' => 'Oops, API Key is Incorrect!');
					$this->response($this->json($respon), 404);
				}
			} else {
				$respon = array( 'status' => 'failed', 'message' => 'Forbidden, API Key is Required!');
				$this->response($this->json($respon), 404);
			}

	}
	
	//code added by mirette
	public function getAllCategories(){
		include "../includes/config.php";
        $setting_qry    = "SELECT * FROM tbl_settings where id = '1'";
		$setting_result = mysqli_query($connect, $setting_qry);
		$settings_row   = mysqli_fetch_assoc($setting_result);
		$api_key    = $settings_row['api_key'];

			if (isset($_GET['api_key'])) {

				$access_key_received = $_GET['api_key'];

				if ($access_key_received == $api_key) {

					if($this->get_request_method() != "GET") $this->response('',406);
					$count_total = $this->get_count_result("SELECT COUNT(DISTINCT cid) FROM tbl_category");

					//$query = "SELECT distinct cid, category_name, category_image FROM tbl_category ORDER BY cid DESC";
					$query = "SELECT DISTINCT c.cid, c.category_name, c.category_image, COUNT(DISTINCT r.nid) as post_count
					  FROM tbl_category c LEFT JOIN tbl_news r ON c.cid = r.cat_id GROUP BY c.cid ORDER BY c.category_order DESC";

					$news = $this->get_list_result($query);
					// print_r($news);die();
					foreach($news as $i=>$obj){
				// 		 print_r($obj['cid']);
						$query_post = "SELECT DISTINCT n.nid, 
						n.news_title, 
						n.cat_id,
						n.news_date, 
						n.news_image, 
						n.news_description,
						n.video_url,
						n.video_id, 
						n.content_type, 
									
						c.category_name, 
						COUNT(DISTINCT r.comment_id) as comments_count

						FROM tbl_news n 

						LEFT JOIN tbl_comments r ON n.nid = r.nid 
						LEFT JOIN tbl_category c ON n.cat_id = c.cid 

						WHERE c.cid =" .''.$obj['cid'].''." 

						GROUP BY n.nid 
						ORDER BY n.nid DESC 
								 
						LIMIT 10 OFFSET 0";

						
						
						$post = $this->get_list_result($query_post);
					
					foreach($post as $key=>$value){
							if ( is_array(json_decode($value['news_image'])))
							{
								$images = json_decode($value['news_image']);
								$image = $images[0];
								unset($images[0]);
								$gallery_object = count($images) ? $images:[];
								$gallery = []; 
								$post[$key]['news_image'] = 'http://qhadath.com/qhadath/newsAppByHouida/upload/'.$image;
								foreach ($gallery_object as $k => $img) {
									$gallery[] = $img;
								}
								$post[$key]['news_gallery'] = $gallery;
							}else
							{
								foreach ($post as $k => $v) {
									$post[$k]['news_image'] = 'http://qhadath.com/qhadath/newsAppByHouida/upload/'.$v['news_image'];
									$post[$k]['news_gallery'] = [];
								}
							}
						}
						
						$news[$i]['news'] = array();
						$news[$i]['news'] = $post;
						
						$countNews = count($post);
						
					}
					$count = count($news);
					$respon = array(
						'status' => 'ok', 'count' => $count, 'categories' => $news
					);
					$this->response($this->json($respon), 200);

				} else {
					$respon = array( 'status' => 'failed', 'message' => 'Oops, API Key is Incorrect!');
					$this->response($this->json($respon), 404);
				}
			} else {
				$respon = array( 'status' => 'failed', 'message' => 'Forbidden, API Key is Required!');
				$this->response($this->json($respon), 404);
			}

	}

	//end of the code

    public function getCategoryPosts() {
		//Code added by mirette 7-10
		include "../includes/config.php";
		//end of added code
		$id = $_GET['id'];
		//Code added by mirette 7-10
		if(isset($_GET['user_id'])){
			
			$user_id = $_GET['user_id'];
		}else{
			$user_id = 0;
		}
			//end of added code
		if($this->get_request_method() != "GET") $this->response('',406);
		$limit = isset($this->_request['count']) ? ((int)$this->_request['count']) : 10;
		$page = isset($this->_request['page']) ? ((int)$this->_request['page']) : 1;

		$offset = ($page * $limit) - $limit;
		$count_total = $this->get_count_result("SELECT COUNT(DISTINCT nid) FROM tbl_news WHERE cat_id = '$id'");

		$query_category = "SELECT distinct cid, category_name, category_image FROM tbl_category WHERE cid = '$id' ORDER BY cid DESC";

		$query_post = "SELECT DISTINCT n.nid, 
						n.news_title, 
						n.cat_id,
						n.news_date, 
						n.news_image, 
						n.news_description,
						n.video_url,
						n.video_id, 
						n.content_type, 
									
						c.category_name, 
						COUNT(DISTINCT r.comment_id) as comments_count

						FROM tbl_news n 

						LEFT JOIN tbl_comments r ON n.nid = r.nid 
						LEFT JOIN tbl_category c ON n.cat_id = c.cid 

						WHERE c.cid = '$id' 

						GROUP BY n.nid 
						ORDER BY n.nid DESC 
								 
						LIMIT $limit OFFSET $offset";

		$category = $this->get_category_result($query_category);
		$post = $this->get_list_result($query_post);
		$count = count($post);
 				foreach ($post as $key => $value) {
					 	//Code added by mirette 7-10
					if ($user_id != 0)
                	{

                		  $sql= "select count(*) as count from tbl_favorites where user_id=$user_id and article_id = ".$value['nid'] ;
                			$cnt = mysqli_query($connect,$sql);  
                			$cnt= mysqli_fetch_assoc($cnt);
                		if ($cnt['count'])
                			$post[$key]['favorite'] = 1;
                		else
                			$post[$key]['favorite'] = 0;

					} else { $post[$key]['favorite'] = 0; }
					//end of added code by mirette
							if ( is_array(json_decode($value['news_image'])))
							{
								$images = json_decode($value['news_image']);
								$image = $images[0];
								unset($images[0]);
								$gallery_object = count($images) ? $images:[];
								$gallery = []; 
								$post[$key]['news_image'] = $image;
								foreach ($gallery_object as $k => $img) {
									$gallery[] = $img;
								}
								$post[$key]['news_gallery'] = $gallery;
							}else
							{
								foreach ($post as $k => $v) {
									$post[$k]['news_image'] = $v['news_image'];
									$post[$k]['news_gallery'] = [];
								}
							}
						}

		$respon = array(
			'status' => 'ok', 'count' => $count, 'count_total' => $count_total, 'pages' => $page, 'category' => $category, 'posts' => $post
		);
		$this->response($this->json($respon), 200);

    }

    public function getSearchResults() {

    	include "../includes/config.php";
		    $setting_qry    = "SELECT * FROM tbl_settings where id = '1'";
		    $setting_result = mysqli_query($connect, $setting_qry);
		   // $settings_row   = mysqli_fetch_assoc($setting_result);
		    //$api_key    = $settings_row['api_key'];


			// if (isset($_GET['api_key'])) {

			// 	$access_key_received = $_GET['api_key'];

			// 	if ($access_key_received == $api_key) {

					$search = $_GET['search'];
					$categoryId = $_GET['categoryId'];
 if($categoryId!=""){
	 // echo "ff"; return;
					if($this->get_request_method() != "GET") $this->response('',406);
					$limit = isset($this->_request['count']) ? ((int)$this->_request['count']) : 10;
					$page = isset($this->_request['page']) ? ((int)$this->_request['page']) : 1;

					$offset = ($page * $limit) - $limit;
					$count_total = $this->get_count_result("SELECT COUNT(DISTINCT n.nid) FROM tbl_news n, tbl_category c, tbl_tags tg WHERE n.cat_id = '$categoryId' AND n.nid = tg.new_id AND (n.news_title LIKE N'%$search%' OR tg.tag_name LIKE N'%$search%')");

					$query = "SELECT DISTINCT n.nid, 
									n.news_title, 
									n.cat_id,
									n.news_date, 
									n.news_image, 
									n.news_description,
									n.video_url,
									n.video_id, 
									n.content_type, 
									
									c.category_name,
									tg.created_at as tags,

									COUNT(DISTINCT r.comment_id) as comments_count

								  FROM tbl_news n

								  LEFT JOIN tbl_comments r ON n.nid = r.nid 
								  LEFT JOIN tbl_category c ON n.cat_id = c.cid 
								  LEFT JOIN tbl_tags tg ON n.nid = tg.new_id 

								  WHERE n.cat_id = '$categoryId' AND (n.news_title LIKE N'%$search%' OR tg.tag_name LIKE N'%$search%') 

								  GROUP BY n.nid 
								  ORDER BY n.nid DESC

							LIMIT $limit OFFSET $offset";

					$post = $this->get_list_result($query);

					foreach ($post as $key => $value) {

						    $qry_tags = "SELECT tag_name FROM tbl_tags WHERE new_id='".$value['nid']."'";
						    $tags_result = mysqli_query($connect, $qry_tags);
						    $json = mysqli_fetch_all ($tags_result, MYSQLI_ASSOC);
						    $tags_array = array_column($json, 'tag_name');
						    $tags_value=implode(',', $tags_array);
                            $post[$key]['tags']=$tags_array;
                           // print_r($value['news_image']); return;
							if ( is_array(json_decode($value['news_image'])))
							{
								$images = json_decode($value['news_image']);
								$image = $images[0];
								unset($images[0]);
								$gallery_object = count($images) ? $images:[];
								$gallery = []; 
								$post[$key]['news_image'] = $image;
								foreach ($gallery_object as $k => $img) {
									$gallery[] = $img;
								}
								$post[$key]['news_gallery'] = $gallery;
							}else
							{
								foreach ($post as $k => $v) {
									$post[$k]['news_image'] = $v['news_image'];
									$post[$k]['news_gallery'] = [];
								}
							}
						}
					$count = count($post);
					$respon = array(
						'status' => 'ok', 'count' => $count, 'count_total' => $count_total, 'pages' => $page, 'posts' => $post
					);
					$this->response($this->json($respon), 200);

				// } else {
				// 	$respon = array( 'status' => 'failed', 'message' => 'Oops, API Key is Incorrect!');
				// 	$this->response($this->json($respon), 404);
				// }
			// } else {
			// 	$respon = array( 'status' => 'failed', 'message' => 'Forbidden, API Key is Required!');
			// 	$this->response($this->json($respon), 404);
			// }
}//the end of if category id
else{
	 // echo "ff"; return;
					if($this->get_request_method() != "GET") $this->response('',406);
					$limit = isset($this->_request['count']) ? ((int)$this->_request['count']) : 10;
					$page = isset($this->_request['page']) ? ((int)$this->_request['page']) : 1;

					$offset = ($page * $limit) - $limit;
					$count_total = $this->get_count_result("SELECT COUNT(DISTINCT n.nid) FROM tbl_news n, tbl_category c, tbl_tags tg WHERE n.cat_id = c.cid AND n.nid = tg.new_id AND (n.news_title LIKE N'%$search%' OR tg.tag_name LIKE N'%$search%')");

					$query = "SELECT DISTINCT n.nid, 
									n.news_title, 
									n.cat_id,
									n.news_date, 
									n.news_image, 
									n.news_description,
									n.video_url,
									n.video_id, 
									n.content_type, 
									
									c.category_name,
									tg.created_at as tags,

									COUNT(DISTINCT r.comment_id) as comments_count

								  FROM tbl_news n

								  LEFT JOIN tbl_comments r ON n.nid = r.nid 
								  LEFT JOIN tbl_category c ON n.cat_id = c.cid 
								  LEFT JOIN tbl_tags tg ON n.nid = tg.new_id 

								  WHERE n.cat_id = c.cid AND (n.news_title LIKE N'%$search%' OR tg.tag_name LIKE N'%$search%') 

								  GROUP BY n.nid 
								  ORDER BY n.nid DESC

							LIMIT $limit OFFSET $offset";

					$post = $this->get_list_result($query);

					foreach ($post as $key => $value) {

						    $qry_tags = "SELECT tag_name FROM tbl_tags WHERE new_id='".$value['nid']."'";
						    $tags_result = mysqli_query($connect, $qry_tags);
						    $json = mysqli_fetch_all ($tags_result, MYSQLI_ASSOC);
						    $tags_array = array_column($json, 'tag_name');
						    $tags_value=implode(',', $tags_array);
                            $post[$key]['tags']=$tags_array;
                           // print_r($value['news_image']); return;
							if ( is_array(json_decode($value['news_image'])))
							{
								$images = json_decode($value['news_image']);
								$image = $images[0];
								unset($images[0]);
								$gallery_object = count($images) ? $images:[];
								$gallery = []; 
								$post[$key]['news_image'] = $image;
								foreach ($gallery_object as $k => $img) {
									$gallery[] = $img;
								}
								$post[$key]['news_gallery'] = $gallery;
							}else
							{
								foreach ($post as $k => $v) {
									$post[$k]['news_image'] = $v['news_image'];
									$post[$k]['news_gallery'] = [];
								}
							}
						}
					$count = count($post);
					$respon = array(
						'status' => 'ok', 'count' => $count, 'count_total' => $count_total, 'pages' => $page, 'posts' => $post
					);
					$this->response($this->json($respon), 200);

				// } else {
				// 	$respon = array( 'status' => 'failed', 'message' => 'Oops, API Key is Incorrect!');
				// 	$this->response($this->json($respon), 404);
				// }
			// } else {
			// 	$respon = array( 'status' => 'failed', 'message' => 'Forbidden, API Key is Required!');
			// 	$this->response($this->json($respon), 404);
			// }
}
    }

    public function getComments() {

			$nid = $_GET['nid'];

			if($this->get_request_method() != "GET") $this->response('',406);
			$count_total = $this->get_count_result("SELECT COUNT(DISTINCT comment_id) FROM tbl_comments c, tbl_news n WHERE n.nid = c.nid AND n.nid = '$nid'");
			
			$query = "SELECT 

			c.comment_id,
			c.user_id,
			u.name,
			u.imageName AS 'image',
			c.date_time,
			c.content

			FROM tbl_news n, tbl_comments c, tbl_users u WHERE n.nid = c.nid AND c.user_id = u.id AND n.nid = '$nid' 

			ORDER BY c.comment_id DESC";
					  
			$categories = $this->get_list_result($query);
			$count = count($categories);
			$respon = array(
				'status' => 'ok', 'count' => $count, 'comments' => $categories
			);
			$this->response($this->json($respon), 200);
	}

	public function postComment() {

			if($_SERVER['REQUEST_METHOD'] == 'POST') {

			       $response = array();
			       //mendapatkan data
			       $nid = $_GET['nid'];
			       $user_id = $_GET['user_id'];
			       $content = $_GET['content'];
			       $date_time = $_GET['date_time'];

			       include "../includes/config.php";

			       $sql = "INSERT INTO tbl_comments (nid, user_id, content, date_time) VALUES('$nid', '$user_id', '$content', '$date_time')";
			       if (mysqli_query($connect, $sql)) {
			         $response["value"] = 1;
			         $response["message"] = "success post comment";
			         echo json_encode($response);
			       } else {
			         $response["value"] = 0;
			         $response["message"] = "oops! failed!";
			         echo json_encode($response);
			       }
			     
			     // tutup database
			     mysqli_close($connect);

			  } else {
			    $response["value"] = 0;
			    $response["message"] = "oops! failed!";

			    header( 'Content-Type: application/json; charset=utf-8' );
			    echo json_encode($response);
			  }

	}

	public function updateComment() {

		    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

		        $response = array();

		        //mendapatkan data
		        $comment_id = $_GET['comment_id'];
		        $date_time  = $_GET['date_time'];
		        $content    = $_GET['content'];

		        include "../includes/config.php";

		        $sql = "UPDATE tbl_comments SET comment_id = '$comment_id', date_time = '$date_time', content = '$content' WHERE comment_id = '$comment_id'";

		        if(mysqli_query($connect, $sql)) {
		            $response["value"] = 1;
		            $response["message"] = "your comment successfully updated";

		            header( 'Content-Type: application/json; charset=utf-8' );
		            echo json_encode($response);
		        } else {
		            $response["value"] = 0;
		            $response["message"] = "oops! failed update comment!";

		            header( 'Content-Type: application/json; charset=utf-8' );
		            echo json_encode($response);
		        }

		        mysqli_close($connect);

		    }

	}

	public function deleteComment() {

		    include "../includes/config.php";

		    if($_SERVER['REQUEST_METHOD'] == 'POST') {
		      
		        $response = array();
		        //mendapatkan data
		        $comment_id = $_GET['comment_id'];
		        $sql = "DELETE FROM tbl_comments WHERE comment_id = '$comment_id'";

		        if(mysqli_query($connect, $sql)) {
		            $response["value"] = 1;
		            $response["message"] = "Your comment was deleted successfully.";

		            header( 'Content-Type: application/json; charset=utf-8' );
		            echo json_encode($response);
		        } else {
		            $response["value"] = 0;
		            $response["message"] = "oops! Failed to delete comment!";

		            header( 'Content-Type: application/json; charset=utf-8' );
		            echo json_encode($response);
		        }
		        mysqli_close($connect);

		    }

	}

		public function userRegister() {

			include "../includes/config.php";
			include "../public/register.php";

			if(isset($_GET['email'])) {
	
				$qry = "SELECT * FROM tbl_users WHERE email = '".$_GET['email']."'"; 
				$sel = mysqli_query($connect, $qry);
			
				if(mysqli_num_rows($sel) > 0) {
					$set['result'][]=array('msg' => "Email address already used!", 'success'=>'0');
					echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
					die();
				} else {
		 			$data = array(
		 			'user_type'=>'Normal',											 
		 			'user_id'=>$_GET['user_id'],											 
					'name'  => $_GET['name'],
					'email'  =>  $_GET['email'],
					'password'  =>  $_GET['password'],
					'status'  =>  '1'
					);

					$qryRes = Insert('tbl_users', $data);

			                    if (!isset($qryRes) || trim($qryRes)==='')
			                    {
			                        $set['result'][] = array('msg' => "Register succesfully...!", 'success'=>'1');
			                        echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
			                        
			                    }
			                    else
			                    {
			                        $set['result'][] = array('msg' => $qryRes, 'success'=>'0');
			                        echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
			                    }
					die();
				}
				
			}
			 elseif (isset($_GET['user_id']))
			{ 
				$qry = "SELECT * FROM tbl_users WHERE user_id = ".$_GET['user_id']." and user_type='".$_GET['user_type']."'"; 
				$sel = mysqli_query($connect, $qry);
			
					if(mysqli_num_rows($sel) > 0) {
						$set['result'][]=array('msg' => "You already have an account. please login instead", 'success'=>'0');
						echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
						die();
					} else {
		 			$data = array(
		 			'user_type'=>$_GET['user_type'],											 
					'name'  => $_GET['name'],
					'email'  =>  $_GET['user_id'].'@'.$_GET['user_type'].'.com',
					'password'  =>  md5($_GET['user_id'].'@'.$_GET['user_type'].'.com'),
					'status'  =>  '1'
					);

					$qryRes = Insert('tbl_users', $data);

			                    if (!isset($qryRes) || trim($qryRes)==='')
			                    {
			                        $set['result'][] = array('msg' => "Register succesfully...!", 'success'=>'1');
			                        echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
			                        
			                    }
			                    else
			                    {
			                        $set['result'][] = array('msg' => $qryRes, 'success'=>'0');
			                        echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
			                    }
					die();
				}
			} 
			 else 
			 {
				
				 header( 'Content-Type: application/json; charset=utf-8' );
				 $json = json_encode($set);

				 echo $json;
				 exit;		 
			}

		}

		public function getUser() {

		    include "../includes/config.php";
		    
		    if(isset($_REQUEST['user_id'])) {
		         
		         $id = $_REQUEST['user_id'];
		         
		         $query = " SELECT * FROM tbl_users WHERE id = '$id' ";
		         $result = mysqli_query($connect, $query);
		         
		         while ($row = mysqli_fetch_assoc($result)) {
		              $output[] = $row;
		         }
		         
		         print(json_encode($output));
		         
		         mysqli_close($connect);

		    } else {

		       $output = "not found";
		       print(json_encode($output));
		    
		    }

		}

		public function getUserLogin() {

			include "../includes/config.php"; 
			include "../public/register.php";
			header('Content-Type: application/json');

			if (isset($_GET['email']))
			{
				$qry = "SELECT * FROM tbl_users WHERE email = '".@$_GET['email']."' AND password = '".@$_GET['password']."'"; 
				$result = mysqli_query($connect, $qry);
				$num_rows = mysqli_num_rows($result);
				$row = mysqli_fetch_assoc($result);
					
			    if ($num_rows > 0 && $row['status'] == 1) { 		 
					$set['result'][] = array('user_id' => $row['id'], 'name' => $row['name'], 'email' => $row['email'], 'success' => '1'); 
				} else if ($num_rows > 0 && $row['status'] == 0) {
					$set['result'][] = array('msg' => 'Account disabled', 'success' => '2');
				} else {
					$set['result'][] = array('msg' => 'Login failed', 'success' => '0');
				}
				 
				header( 'Content-Type: application/json; charset=utf-8' );
				$json = json_encode($set);

				echo $json;
				exit;
			}
			else
			{
				$qry = "SELECT * FROM tbl_users WHERE user_id = '".@$_GET['user_id']."' AND user_type = '".@$_GET['user_type']."'"; 
				$result = mysqli_query($connect, $qry);
				$num_rows = mysqli_num_rows($result);
				$row = mysqli_fetch_assoc($result);
					
			    if ($num_rows > 0 && $row['status'] == '1') { 		 
					$set['result'][] = array('user_id' => $row['id'], 'name' => $row['name'],  'success' => '1'); 
				} else if ($num_rows > 0 && $row['status'] == 0) {
					$set['result'][] = array('msg' => 'Account disabled', 'success' => '2');
				} elseif (!$num_rows){
						$qry = "SELECT * FROM tbl_users WHERE user_id = ".$_GET['user_id']." and user_type='".$_GET['user_type']."'"; 
						$sel = mysqli_query($connect, $qry);
				
						if(mysqli_num_rows($sel) > 0) {
							$set['result'][]=array('msg' => "Login successful", 'success'=>'0');
							echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
							die();
						} else {
			 			$data = array(
			 			'user_type'=>$_GET['user_type'],											 
			 			'user_id'=>$_GET['user_id'],											 
						'name'  => $_GET['name'],
						'email'  =>  $_GET['user_id'].'@'.$_GET['user_type'].'.com',
						'password'  =>  md5($_GET['user_id'].'@'.$_GET['user_type'].'.com'),
						'status'  =>  '1'
						);

						$qryRes = Insert('tbl_users', $data);

				                    if (!isset($qryRes) || trim($qryRes)==='')
				                    {
				                    	$qry = "SELECT * FROM tbl_users WHERE user_id = '".@$_GET['user_id']."' AND user_type = '".@$_GET['user_type']."'"; 
										$result = mysqli_query($connect, $qry);
										$num_rows = mysqli_num_rows($result);
										$row = mysqli_fetch_assoc($result);

				                        $set['result'][] = array('user_id' => $row['id'], 'name' => $row['name'],  'success'=>'1');
				                        echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
				                        
				                    }
				                    else
				                    {
				                        $set['result'][] = array('msg' => $qryRes, 'success'=>'0');
				                        echo $val=   json_encode($set, JSON_UNESCAPED_UNICODE) ;
				                    }
						die();
					}
				} else {
					$set['result'][] = array('msg' => 'Login failed', 'success' => '0');
				}
				 
				header( 'Content-Type: application/json; charset=utf-8' );
				$json = json_encode($set);

				echo $json;
				exit;
			}
			
		}

		public function getUserProfile() {

			include "../includes/config.php";

			$id = $_GET['id'];

 	 		$qry = "SELECT * FROM tbl_users WHERE id = '$id' ";
			$result = mysqli_query($connect, $qry);	 
			$row = mysqli_fetch_assoc($result);
			  				 
			$set['result'][] = array(
				'user_id' => $row['id'],
				'name'=>$row['name'],
				'email'=>$row['email'],
				'password'=>$row['password'],
				'image'=>$row['imageName'],
				'success'=>'1'
			);

			header( 'Content-Type: application/json; charset=utf-8' );
			$json = json_encode($set);

			echo $json;
			exit;

		}

		public function updateUserProfile() {

			include "../includes/config.php";
			include "../public/register.php";
	
		 	if($_GET['password']!="") {
				$data = array(
				'name'  =>  $_GET['name'],
				'email'  =>  $_GET['email'],
				'password'  =>  $_GET['password']
				);
			} else {
				$data = array(
				'name'  =>  $_GET['name'],
				'email'  =>  $_GET['email']
				);
			}
				
			$user_edit = Update('tbl_users', $data, "WHERE id = '".$_GET['user_id']."'");
		 	$set['result'][] = array('msg'=>'Updated', 'success'=>'1');
					 
			header( 'Content-Type: application/json; charset=utf-8' );
			$json = json_encode($set);

			echo $json;
			exit;

		}

		public function updateUserPhoto() {

			include "../includes/config.php";
		
			// check if "image" abd "user_id" is set 
			if(isset($_GET["image"]) && isset($_GET["user_id"])) {
	
				$data = $_GET["image"];
				// print_r($data);die();
				$time = time();
	
				$user_id = $_GET["user_id"];
				//$oldImage ="images/"."1_1497679518.jpg";
				$ImageName = $user_id.'_'.$time.".jpg";
	
				//$filePath = "images/".$ImageName;
				$filePath = '../upload/avatar/'.$ImageName; // path of the file to store
				echo "file : ".$filePath;
				//echo "unlink : ".$oldImage;
	
				// check if file exits
				if (file_exists($filePath)) {
					unlink($filePath); // delete the old file
				} 
				// create a new empty file
				$myfile = fopen($filePath, "w") or die("Unable to open file!");
				// add data to that file
				file_put_contents($filePath, base64_decode($data));
	
				// update the Customer table with new image name.
				$query = " UPDATE tbl_users SET imageName = '$ImageName' WHERE id = '$user_id' ";
				mysqli_query($connect, $query);
	
				
			} else {
				echo 'not set';
			}
			
			mysqli_close($connect);
	
		}
		//end of added code by mirette

	public function forgotPassword() {

		include "../includes/config.php";
		    
		$qry = "SELECT * FROM tbl_users WHERE email = '".$_GET['email']."'"; 
		$result = mysqli_query($connect, $qry);
		$row = mysqli_fetch_assoc($result);
		
		if ($row['email']!="") {
			//$new_password=rand(1,99999);

			$to = $_GET['email'];
			// subject
			$subject = '[IMPORTANT] Android News App Forgot Password Information';
			//$message = '<div><strong>Confirmation Code</strong>:'.$confirm_code.'<br></div>';
			
			$message='<div style="background-color: #f9f9f9;" align="center"><br />
					  <table style="font-family: OpenSans,sans-serif; color: #666666;" border="0" width="600" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF">
					    <tbody>
					      <tr>
					        <td width="600" valign="top" bgcolor="#FFFFFF"><br>
					          <table style="font-family:OpenSans,sans-serif; color: #666666; font-size: 10px; padding: 15px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="left">
					            <tbody>
					              <tr>
					                <td valign="top"><table border="0" align="left" cellpadding="0" cellspacing="0" style="font-family:OpenSans,sans-serif; color: #666666; font-size: 10px; width:100%;">
					                    <tbody>
					                      <tr>
					                        <td><p style="color: #262626; font-size: 28px; margin-top:0px;"><strong>Dear '.$row['name'].'</strong></p>
					                          <p style="color:#262626; font-size:20px; line-height:32px;font-weight:500;">Thank you for using Android News App,<br>
					                            Your password is: '.$row['password'].'</p>
					                          <p style="color:#262626; font-size:20px; line-height:32px;font-weight:500;margin-bottom:30px;">Thanks you,<br />
					                            Android News App.</p></td>
					                      </tr>
					                    </tbody>
					                  </table></td>
					              </tr>
					               
					            </tbody>
					          </table></td>
					      </tr>
					      <tr>
					        <td style="color: #262626; padding: 20px 0; font-size: 20px; border-top:5px solid #52bfd3;" colspan="2" align="center" bgcolor="#ffffff">Copyright © Android News App.</td>
					      </tr>
					    </tbody>
					  </table>
					</div>';
 
			$headers = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: Android News App <don-not-reply@solodroid.net>' . "\r\n";
			// Mail it
			@mail($to, $subject, $message, $headers);

			$set['result'][]=array('msg' => "Password has been sent on your mail!",'success'=>'1');

		} else {
			$set['result'][]=array('msg' => "Email not found in our database!",'success'=>'0');		
		}

	 	header( 'Content-Type: application/json; charset=utf-8');
	    $json = json_encode($set);
					
		echo $json;
		exit;

	}

	public function getPrivacyPolicy() {

		include "../includes/config.php";
		
		$sql = "SELECT * FROM tbl_settings WHERE id = 1";
		$result = mysqli_query($connect, $sql);

		header( 'Content-Type: application/json; charset=utf-8' );
		print json_encode(mysqli_fetch_assoc($result));


	}




    public function get_list_result($query) {
		$result = array();
		$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
		if($r->num_rows > 0) {
			while($row = $r->fetch_assoc()) {
				$result[] = $row;
			}
		}
		return $result;
	}

    public function get_count_result($query) {
		$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
		if($r->num_rows > 0) {
			$result = $r->fetch_row();
			return $result[0];
		}
		return 0;
	}

	private function get_category_result($query) {
		$result = array();
		$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
		if($r->num_rows > 0) {
			while($row = $r->fetch_assoc()) {
				$result = $row;
			}
		}
		return $result;
	}

	private function get_one($query) {
		$result = array();
		$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
		if($r->num_rows > 0) $result = $r->fetch_assoc();
		return $result;
	}
    



	//new code added by mirette for register from website angular site 9-12-2019
	public function userRegisterWebsite(){
		include "../includes/config.php";
		$arrData = file_get_contents('php://input');
		// print($arrData);die();
		$Data = json_decode($arrData);
		
		// print_r($Data->username);die();
		if(isset($Data->email)) {
			
			$qry = "SELECT * FROM tbl_users WHERE email = '".$Data->email."'"; 
			// print($qry);die();
			$sel = mysqli_query($connect, $qry);
			$count = mysqli_num_rows($sel);
			if($count > 0) {
				$set['result'][]=array('msg' => "Email address already used!", 'success'=>'0');
				echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
				die();
			} else {
				$sql = "insert into tbl_users (email,name,password) values('$Data->email','$Data->username','$Data->password')";
				// print($sql);die();
				$insert = mysqli_query($connect,$sql);
					if($insert){
					$set['result'][] = array('msg' => "Register succesfully...!", 'success'=>'1');
					echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
					
				}
				else
				{
					$set['result'][] = array('msg' => $sql, 'success'=>'0');
					echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
				}
				die();
			}
			
		}else{
			
			 header( 'Content-Type: application/json; charset=utf-8' );
			 $json = json_encode($set);

			 echo $json;
			 exit;		 
		}

	}
	//end added code by mirette

	//add code by mirette 15-9-2019 login function for website

	public function userLoginWebsite(){
		include "../includes/config.php";
		$arrData = file_get_contents('php://input');
		// print($arrData);die();
		$Data = json_decode($arrData);

		$qry = "SELECT * FROM tbl_users WHERE email = '".@$Data->email."' AND password = '".@$Data->password."'"; 
				$result = mysqli_query($connect, $qry);
				$num_rows = mysqli_num_rows($result);
				$row = mysqli_fetch_assoc($result);
					
			    if ($num_rows > 0 && $row['status'] == 1) { 		 
					$set['result'][] = array('user_id' => $row['id'], 'name' => $row['name'], 'email' => $row['email'], 'success' => '1'); 
				} else if ($num_rows > 0 && $row['status'] == 0) {
					$set['result'][] = array('msg' => 'Account disabled', 'success' => '2');
				} else {
					$set['result'][] = array('msg' => 'Login failed', 'success' => '0');
				}
				 
				header( 'Content-Type: application/json; charset=utf-8' );
				$json = json_encode($set);

				echo $json;
				exit;


	}

public function updateUserWebsite(){
	include "../includes/config.php";
	
		$arrData = file_get_contents('php://input');
		// print($arrData);die();
		$Data = json_decode($arrData);
		if($Data->password = ''){
			$sql = "UPDATE tbl_users SET `name` = '".$Data->name."', email = '".$Data->email."', password = '".$Data->password."' WHERE id = ".$Data->user_id."";
		}else{
			$sql = "UPDATE tbl_users SET `name` = '".$Data->name."', email = '".$Data->email."' WHERE id = ".$Data->user_id."";
		}
		
		if(mysqli_query($connect, $sql)) {
            $set['fcm_push_notification'][] = array('msg' => 'Success','Success'=>'1');
            echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
            die();
        }else{
            $set['fcm_push_notification'][] = array('msg' => 'error while update','Success'=>'0');
            echo $val= str_replace('\\/', '/', json_encode($set, JSON_UNESCAPED_UNICODE));
            die();
        }
}

	//end added code by mirette

	//new added code by mirette upload photo from website date 16-9
	public function updateUserPhotoWeb() {

		include "../includes/config.php";
		// $arrData = file_get_contents('php://input');
		// $Data = json_decode($arrData);
		//  print_r($_FILES['image']);die();

		 if(isset($_FILES["file"]["type"]))
		 {
		 $validextensions = array("jpeg", "jpg", "png");
		 $temporary = explode(".", $_FILES["file"]["name"]);
		 $file_extension = end($temporary);
		 if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
		 ) && ($_FILES["file"]["size"] < 100000)//Approx. 100kb files can be uploaded.
		 && in_array($file_extension, $validextensions)) {
		 if ($_FILES["file"]["error"] > 0)
		 {
		 echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
		 }
		 else
		 {
		 if (file_exists("upload/" . $_FILES["file"]["name"])) {
		 echo $_FILES["file"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
		 }
		 else
		 {
		 $sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
		 $targetPath = "../upload/avatar/".$_FILES['file']['name']; // Target path where file is to be stored
		 move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
		 $user_id =  $_POST['user_id'];
		 $file_name = "/upload/avatar/".$_FILES["file"]["name"];
		 $query = " UPDATE tbl_users SET imageName = '".$file_name."' WHERE id = $user_id";
		//  print($query);die();
		 mysqli_query($connect, $query);
		 echo "<span id='success'>Image Uploaded Successfully...!!</span><br/>";
		 echo "<br/><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
		 echo "<b>Type:</b> " . $_FILES["file"]["type"] . "<br>";
		 echo "<b>Size:</b> " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
		 echo "<b>Temp file:</b> " . $_FILES["file"]["tmp_name"] . "<br>";
		 }
		 }
		 }
		 else
		 {
		 echo "<span id='invalid'>***Invalid file Size or Type***<span>";
		 }
		 }	



		// check if "image" abd "user_id" is set 
		// if(isset($_GET["image"]) && isset($_GET["user_id"])) {

		// 	$data = $_GET["image"];
		// 	// print_r($data);die();
		//     $time = time();

		//     $user_id = $_GET["user_id"];
		//     //$oldImage ="images/"."1_1497679518.jpg";
		//     $ImageName = $user_id.'_'.$time.".jpg";

		//     //$filePath = "images/".$ImageName;
		//     $filePath = '../upload/avatar/'.$ImageName; // path of the file to store
		//     echo "file : ".$filePath;
		//     //echo "unlink : ".$oldImage;

		//     // check if file exits
		//     if (file_exists($filePath)) {
		//         unlink($filePath); // delete the old file
		//     } 
		//     // create a new empty file
		//     $myfile = fopen($filePath, "w") or die("Unable to open file!");
		//     // add data to that file
		//     file_put_contents($filePath, base64_decode($data));

		//     // update the Customer table with new image name.
		//     $query = " UPDATE tbl_users SET imageName = '$ImageName' WHERE id = '$user_id' ";
		//     mysqli_query($connect, $query);

			
		// } else {
		//     echo 'not set';
		// }
		
		// mysqli_close($connect);

	}


}