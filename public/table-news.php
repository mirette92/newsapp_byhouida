<?php
include 'functions.php';
include 'fcm.php';
?>

<?php

$sql_user   = "SELECT COUNT(*) as num FROM tbl_fcm_token";
$total_user = mysqli_query($connect, $sql_user);
$total_user = mysqli_fetch_array($total_user);
$total_user = $total_user['num'];

if (isset($_GET['send_notification_post'])) {

    $qry = "SELECT * FROM tbl_news WHERE nid = '".$_GET['send_notification_post']."'";
    $result = mysqli_query($connect, $qry);
    $row = mysqli_fetch_assoc($result);

    $pesan = $row['news_title'];
    $id = $row['nid'];
    $link = "";

    if ($row['content_type'] == 'youtube') {
        $image = 'http://img.youtube.com/vi/'.$row['video_id'].'/mqdefault.jpg';
    } else {
        $image = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/upload/'.$row['news_image'];
    }

    $users_sql = "SELECT * FROM tbl_fcm_token";

    $users_result = mysqli_query($connect, $users_sql);
    while($user_row = mysqli_fetch_assoc($users_result)) {

        $msg = $pesan;
        $img = $image;
        $id = $id;
        $link = $link;

        $data = array("title" => $msg, "image" => $img, "id" => $id, "link" => $link,'type'=>'default');

        echo SEND_FCM_NOTIFICATION($user_row['token'], $data);

    }

    if ($result) {
        $error['push_notification'] = "<div class='alert alert-info'>Congratulations, Push Notification Sent to $total_user Users.</div>";
    } else {
        $error['push_notification'] = "<div>Failed.</div>";
    }
}
if (isset($_GET['send_notification_urgent_post'])) {

    $qry = "SELECT * FROM tbl_news WHERE nid = '".$_GET['send_notification_urgent_post']."'";
    $result = mysqli_query($connect, $qry);
    $row = mysqli_fetch_assoc($result);

    $pesan = $row['news_title'];
    $id = $row['nid'];
    $link = "";

    if ($row['content_type'] == 'youtube') {
        $image = 'http://img.youtube.com/vi/'.$row['video_id'].'/mqdefault.jpg';
    } else {
        $image = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/upload/'.$row['news_image'];
    }

    $users_sql = "SELECT * FROM tbl_fcm_token";

    $users_result = mysqli_query($connect, $users_sql);
    while($user_row = mysqli_fetch_assoc($users_result)) {

        $msg = $pesan;
        $img = $image;
        $id = $id;
        $link = $link;

        $data = array("title" => $msg, "image" => $img, "id" => $id, "link" => $link,'type'=>'urgent');

        echo SEND_FCM_NOTIFICATION($user_row['token'], $data);

    }

    if ($result) {
        $error['push_notification'] = "<div class='alert alert-info'>Congratulations, Push Notification Sent to $total_user Users.</div>";
    } else {
        $error['push_notification'] = "<div>Failed.</div>";
    }
}
if (isset($_GET['send_notification_goal_post'])) {

    $qry = "SELECT * FROM tbl_news WHERE nid = '".$_GET['send_notification_goal_post']."'";
    $result = mysqli_query($connect, $qry);
    $row = mysqli_fetch_assoc($result);

    $pesan = $row['news_title'];
    $id = $row['nid'];
    $link = "";

    if ($row['content_type'] == 'youtube') {
        $image = 'http://img.youtube.com/vi/'.$row['video_id'].'/mqdefault.jpg';
    } else {
        $image = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/upload/'.$row['news_image'];
    }

    $users_sql = "SELECT * FROM tbl_fcm_token";

    $users_result = mysqli_query($connect, $users_sql);
    while($user_row = mysqli_fetch_assoc($users_result)) {

        $msg = $pesan;
        $img = $image;
        $id = $id;
        $link = $link;

        $data = array("title" => $msg, "image" => $img, "id" => $id, "link" => $link,'type'=>'goal');

        echo SEND_FCM_NOTIFICATION($user_row['token'], $data);

    }

    if ($result) {
        $error['push_notification'] = "<div class='alert alert-info'>Congratulations, Push Notification Sent to $total_user Users.</div>";
    } else {
        $error['push_notification'] = "<div>Failed.</div>";
    }
}

?>

<?php
// create object of functions class
$function = new functions;

// create array variable to store data from database
$data = array();

if(isset($_GET['category_id'])){
    $category_id = $function->sanitize($_GET['category_id']);
    $bind_category_id = $category_id;
} else {
    $category_id = "";
    $bind_category_id = $category_id;
}
if(isset($_GET['keyword'])) {
    // check value of keyword variable
    $keyword = $function->sanitize($_GET['keyword']);
    $bind_keyword = "%".$keyword."%";
} else {
    $keyword = "";
    $bind_keyword = $keyword;
}

$sql_query = "SELECT nid,urgent, news_title, news_image, news_date, category_name, video_id, content_type,u.username FROM tbl_news m  LEFT JOIN tbl_admin u ON u.id=m.add_by LEFT JOIN tbl_category c ON  m.cat_id = c.cid WHERE 1 ";
if (!empty($keyword)) {
    $sql_query .= " AND news_title LIKE ? ";
}
if(!empty($category_id)){
    $sql_query.=" AND m.cat_id=?";
}
$sql_query.=" ORDER BY m.nid DESC";
//echo $sql_query.'<br>';
$stmt = $connect->stmt_init();
if ($stmt->prepare($sql_query)) {
    // Bind your variables to replace the ?s
    if (!empty($keyword)&&!empty($category_id)) {
        $stmt->bind_param('ss', $bind_keyword,$bind_category_id);
    }elseif (!empty($category_id)&&empty($keyword)) {
        $stmt->bind_param('s', $bind_category_id);
    }elseif (!empty($keyword)&&empty($category_id)) {
        $stmt->bind_param('s', $bind_keyword);
    }

    // Execute query
    $stmt->execute();
    // store result
    $stmt->store_result();
    $stmt->bind_result(
        $data['nid'],
        $data['urgent'],
        $data['news_title'],
        $data['news_image'],
        $data['news_date'],
        $data['category_name'],
        $data['video_id'],
        $data['content_type'],
        $data['username']
    );
    // get total records
    $total_records = $stmt->num_rows;
}

// check page parameter
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}

// number of data that will be display per page
$offset = 10;

//lets calculate the LIMIT for SQL, and save it $from
if ($page) {
    $from 	= ($page * $offset) - $offset;
} else {
    //if nothing was given in page request, lets load the first page
    $from = 0;
}
$sql_query = "SELECT nid,urgent, news_title, news_image, news_date, category_name, video_id, content_type,u.username FROM tbl_news m  LEFT JOIN tbl_admin u ON u.id=m.add_by LEFT JOIN tbl_category c ON  m.cat_id = c.cid WHERE 1 ";
if (!empty($keyword)) {
    $sql_query .= " AND news_title LIKE ? ";
}
if (!empty($category_id)) {
    $sql_query .= " AND m.cat_id = ? ";
}
$sql_query.=" ORDER BY m.nid DESC LIMIT ?, ?";
//echo $sql_query.'<br>';
$stmt_paging = $connect->stmt_init();
if ($stmt_paging ->prepare($sql_query)) {
    // Bind your variables to replace the ?s
    if (!empty($keyword)&&!empty($category_id)) {
        $stmt_paging->bind_param('ssss', $bind_keyword,$bind_category_id, $from, $offset);
    }elseif (!empty($category_id)&&empty($keyword)) {
        $stmt_paging->bind_param('sss', $bind_category_id, $from, $offset);
    }elseif (!empty($keyword)&&empty($category_id)) {
        $stmt_paging->bind_param('sss', $bind_keyword, $from, $offset);
    }else{
        $stmt_paging->bind_param('ss', $from, $offset);
    }
    // Execute query
    $stmt_paging ->execute();
    // store result
    $stmt_paging ->store_result();
    $stmt_paging->bind_result(
        $data['nid'],
        $data['urgent'],
        $data['news_title'],
        $data['news_image'],
        $data['news_date'],
        $data['category_name'],
        $data['video_id'],
        $data['content_type'],
        $data['username']
    );
    //echo $stmt_paging->num_rows;
    // for paging purpose
    $total_records_paging = $total_records;
}
$category=[];
$query="SELECT * FROM tbl_category";
$stmt_category = $connect->stmt_init();
if ($stmt_category ->prepare($query)) {
    // Execute query
    $stmt_category ->execute();
    // store result
    $stmt_category ->store_result();
    $stmt_category->bind_result(
        $category['cid'],
        $category['wp_id'],
        $category['category_name'],
        $category['category_image'],
        $category['category_order']
    );
}
$categorySelected=[];
$query="SELECT * FROM tbl_category";
$stmt_categorySelected = $connect->stmt_init();
if ($stmt_categorySelected ->prepare($query)) {
    // Execute query
    $stmt_categorySelected ->execute();
    // store result
    $stmt_categorySelected ->store_result();
    $stmt_categorySelected->bind_result(
        $category['cid'],
        $category['wp_id'],
        $category['category_name'],
        $category['category_image'],
        $category['category_order']
    );
}
// if no data on database show "No Reservation is Available"
if ($total_records_paging == 0) {

    ?>

    <section class="content">

        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li class="active">Manage News</a></li>
        </ol>

        <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>MANAGE NEWS</h2>
                            <div class="header-dropdown m-r--5">
                                <a href="add-news.php"><button type="button" class="btn bg-blue waves-effect">ADD NEW NEWS</button></a>
                            </div>
                        </div>

                        <div class="body table-responsive">

                            <form method="get">
                                <div class="col-sm-5">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="keyword" placeholder="Search by title...">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group form-float">
                                        <select name="category_id" class="form-control" id="category_id">
                                            <option value="">Select Category</option>
                                            <option value="">Select Category</option>
                                            <?php  while ($stmt_category->fetch()) { ?>
                                                <option <?php if($category_id==$category['cid']){?> selected="selected" <?php }?> value="<?=$category['cid']?>"><?=$category['category_name']?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" name="btnSearch" class="btn bg-blue btn-circle waves-effect waves-circle waves-float"><i class="material-icons">search</i></button>
                                </div>
                            </form>

                            <table class='table table-hover table-striped'>
                                <thead>
                                <tr>
                                    <th width="40%">News Title</th>
                                    <th width="15%">News Image</th>
                                    <th width="15%">Date</th>
                                    <th width="15%">Category</th>
                                    <th width="15%">Add by</th>
                                    <th width="15%">Action</th>
                                </tr>
                                </thead>


                            </table>

                            <div class="col-sm-10">Wopps! No data found with the keyword you entered.</div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
    // otherwise, show data
} else {
$row_number = $from + 1;
?>

<section class="content">

    <ol class="breadcrumb">
        <li><a href="dashboard.php">Dashboard</a></li>
        <li class="active">Manage News</a></li>
    </ol>

    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>MANAGE NEWS</h2>
                        <div class="header-dropdown m-r--5">
                            <a href="add-news.php"><button type="button" class="btn bg-blue waves-effect">ADD NEW NEWS</button></a>
                        </div>
                        <br>
                        <?php echo isset($error['push_notification']) ? $error['push_notification'] : '';?>
                    </div>

                    <div class="body table-responsive">

                        <form method="get">
                            <div class="col-sm-3">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="keyword" placeholder="Search by title...">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <select name="category_id" class="form-control" id="category_id">
                                            <option value="">Select Category</option>
                                            <option value="">Select Category</option>
                                            <?php while ($stmt_category->fetch()) { ?>
                                                <option <?php if($category_id==$category['cid']){?> selected="selected" <?php }?> value="<?=$category['cid']?>"><?=$category['category_name']?></option>
                                            <?php }?>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" name="btnSearch" class="btn bg-blue btn-circle waves-effect waves-circle waves-float"><i class="material-icons">search</i></button>
                            </div>
                        </form>
                            <div class="col-sm-4">
                                <div class="form-group form-float">
                                    <div class="input-group">
                                        <div class="form-line">
                                            <select name="change_category_id" class="form-control" id="change_category_id">
                                                <option value="">Select Category</option>
                                                <option value="">Select Category</option>
                                                <?php while ($stmt_categorySelected->fetch()) { ?>
                                                    <option value="<?=$category['cid']?>"><?=$category['category_name']?></option>
                                                <?php }?>
                                            </select>
                                        </div>

                                        <span class="input-group-btn">
                                            <button id="change-categories-selected" class="btn btn-success input-circle-right" type="button"style="height: 35px;line-height: 17.5px;">
                                            <i class="fa fa-chain-broken"></i> Change</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        <div class="clearfix"></div>
                        <?php if($_SESSION['userID']==1){?>
                        <div class="col-lg-10">
                            <div class="checkbox">
                                <input type="checkbox" name="remove" class="select-all-news" id="select-all-news" value="1">
                                <label for="select-all-news">
                                    Select All
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <button id="remove-news-selected" class="btn btn-danger"><i class="material-icons">delete</i></button>
                        </div>
                        <?php }?>
                        <table class='table table-hover table-striped'>
                            <thead>
                            <tr>
                                <th width="1%">#</th>
                                <th width="30%">News Title</th>
                                <th width="12%">News Image</th>
                                <th width="12%">Date</th>
                                <th width="12%">Category</th>
                                <th width="12%">Add By</th>
                                <th width="10%">Type</th>
                                <th width="23%"><center>Action</center></th>
                            </tr>
                            </thead>

                            <?php
                            while ($stmt_paging->fetch()) { ?>
                                <tr id="remove-single-news-<?=$data['nid']?>">
                                    <td>
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <input type="checkbox" name="remove-news" class="remove-news" id="remove-news-<?=$data['nid']?>" value="<?=$data['nid']?>">
                                                <label for="remove-news-<?=$data['nid']?>">
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td><?php echo $data['news_title'];?></td>

                                    <td>
                                        <?php
                                        if ($data['content_type'] == 'youtube') {
                                            ?>
                                            <img src="https://img.youtube.com/vi/<?php echo $data['video_id'];?>/mqdefault.jpg" height="48px" width="60px"/>
                                        <?php } else { ?>
                                            <img src="upload/<?php echo is_array(json_decode($data['news_image'])) ? json_decode($data['news_image'])[0]:$data['news_image'];?>" height="48px" width="60px"/>
                                        <?php } ?>
                                    </td>

                                    <td>
                                        <?php
                                        
                                        $date = strtotime($data['news_date']);
                                        $new_date = date("F d, Y H:i:s", $date);
                                        echo $new_date;
                                        ?>
                                    </td>
                                    <td class="category-name"><?php echo $data['category_name'];?></td>
                                    <td><?php echo $data['username'];?></td>
                                    <td>
                                        <?php if ($data['content_type'] == 'Post') { ?>
                                            <span class="label bg-green">NEWS</span>
                                        <?php } else { ?>
                                            <span class="label bg-red">VIDEO</span>
                                        <?php } ?>
                                        <?php if($data['urgent']){?>
                                            <span class="label bg-red">URGENT</span>
                                        <?php }?>
                                    </td>
                                    <td><center>
                                            <a href="manage-news.php?send_notification_goal_post=<?php echo $data['nid'];?>" data-toggle="tooltip" data-placement="top" title="Send Goal Notification" onclick="return confirm('Send this notification to your users as goal?')">
                                                <i class="material-icons" style="color:red;">notifications_active</i>
                                            </a>
                                            <a href="manage-news.php?send_notification_post=<?php echo $data['nid'];?>" data-toggle="tooltip" data-placement="top" title="Send Notification" onclick="return confirm('Send this notification to your users?')">
                                                <i class="material-icons">notifications_active</i>
                                            </a>

                                            <a href="news-detail.php?id=<?php echo $data['nid'];?>">
                                                <i class="material-icons">launch</i>
                                            </a>

                                            <a href="edit-news.php?id=<?php echo $data['nid'];?>">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                            <?php if($_SESSION['userID']==1){?>
                                            <a href="delete-news.php?id=<?php echo $data['nid'];?>" onclick="return confirm('Are you sure want to delete this News?')" >
                                                <i class="material-icons">delete</i>
                                            </a>
                                            <?php }?>
                                        </center>

                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>

                        <h4><?php $function->doPages($offset, 'manage-news.php', '', $total_records, $keyword,$category_id); ?></h4>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>