<?php

//    date_default_timezone_set("Africa/Cairo");
    //database configuration
    $host       = "localhost";
    $user       = "root";
    $pass       = "";
    $database   = "news_app_test";

    $connect = new mysqli($host, $user, $pass, $database);

    if (!$connect) {
        die ("connection failed: " . mysqli_connect_error());
    } else {
        $connect->set_charset('utf8');
        // mysqli_query($connect,"SET time_zone='Africa/Cairo'");
    }
	
	$GLOBALS['config'] = $connect;


    $ENABLE_RTL_MODE = 'false';

?>