<?php

    class functions {
        
        function get_random_string($valid_chars, $length){
    
            // start with an empty random string
            $random_string = "";

            // count the number of chars in the valid chars string so we know how many choices we have
            $num_valid_chars = strlen($valid_chars);

            // repeat the steps until we've created a string of the right length
            for ($i = 0; $i < $length; $i++)
            {
                // pick a random number from 1 up to the number of valid chars
                $random_pick = mt_rand(1, $num_valid_chars);

                // take the random character out of the string of valid chars
                // subtract 1 from $random_pick because strings are indexed starting at 0, and we started picking at 1
                $random_char = $valid_chars[$random_pick-1];

                // add the randomly-chosen char onto the end of our string so far
                $random_string .= $random_char;
            }

            // return our finished random string
            return $random_string;
        }// end of get_random_string()
        
        function sanitize($string) {
            include 'includes/config.php';
            // check string value
            $string = mysqli_escape_string($connect, trim(strip_tags(stripslashes($string))));
            return $string;
        }// end of sanitize()
        
        function check_integer($which) {
            if(isset($_GET[$which])){
                if (intval($_GET[$which])>0) {
                    return intval($_GET[$which]);
                } else {
                    return false;
                }
            }
            return false;
        }//end of check_integer()

        function get_current_page() {
            if(($var=$this->check_integer('page'))) {
                //return value of 'page', in support to above method
                return $var;
            } else {
                //return 1, if it wasnt set before, page=1
                return 1;
            }
        }//end of method get_current_page()
        
        function doPages($page_size, $thepage, $query_string, $total=0, $keyword,$category_id='') {
            //per page count
            $index_limit = 10;
            
            //set the query string to blank, then later attach it with $query_string
            $query = '';
            
            if( strlen($query_string) > 0) {
                $query = "&amp;".$query_string;
            }
                
            //get the current page number example: 3, 4 etc: see above method description
            $current = $this->get_current_page();
            
            $total_pages = ceil($total / $page_size);
            $start = max($current - intval($index_limit / 2), 1);
            $end = $start + $index_limit - 1;

            echo '<div class="body right">';
            echo '<ul class="pagination">';

            if ($current == 1) {
                echo '<li class="disabled"><a><i class="material-icons">chevron_left</i></a></li>';
            } else {
                $i = $current - 1;
                echo '<li><a href="'.$thepage.'?page='.$i.$query.'&keyword='.$keyword.'&category_id='.$category_id.'" rel="nofollow" title="go to page '.$i.'"><i class="material-icons">chevron_left</i></a></li>';
                //echo '<p>...</p>&nbsp;';
            }
                //<button>'.$i.'</button>
            if ($start > 1) {
                $i = 1;
                echo '<li><a href="'.$thepage.'?page='.$i.$query.'&keyword='.$keyword.'&category_id='.$category_id.'" title="go to page '.$i.'">'.$i.'</a></li>';
            }

            for ($i = $start; $i <= $end && $i <= $total_pages; $i++) {
                if ($i == $current) {
                    echo '<li class="active"><a>'.$i.'</a></li>';
                } else {
                    echo '<li><a href="'.$thepage.'?page='.$i.$query.'&keyword='.$keyword.'&category_id='.$category_id.'" title="go to page '.$i.'">'.$i.'</a></li>';
                }
            }

            if ($total_pages > $end) {
                $i = $total_pages;
                echo '<li><a href="'.$thepage.'?page='.$i.$query.'&keyword='.$keyword.'&category_id='.$category_id.'" title="go to page '.$i.'">'.$i.'</a></li>';
            }

            if ($current < $total_pages) {
                $i = $current + 1;
                //echo '<p>...</p>&nbsp;';
                echo '<li><a href="'.$thepage.'?page='.$i.$query.'&keyword='.$keyword.'&category_id='.$category_id.'" rel="nofollow" title="go to page '.$i.'"><i class="material-icons">chevron_right</i></a></li>';
            } else {
                echo '<li class="disabled"><a><i class="material-icons">chevron_right</i></a></li>';
            }
            
            echo '</ul>';

            //if nothing passed to method or zero, then dont print result, else print the total count below:       
            if ($total != 0) {
                //prints the total result count just below the paging
                echo '<br><div class="right"><h4>( total '.$total.' )</h1></div></div>';
            } else {
                echo '</div>';
            };
         
        }//end of method doPages()

        public static function reArrayFiles(&$file_post) {

            $file_ary = array();
            $file_count = count($file_post['name']);
            if($file_count){
                $file_keys = array_keys($file_post);

                for ($i=0; $i<$file_count; $i++) {
                    foreach ($file_keys as $key) {
                        $file_ary[$i][$key] = $file_post[$key][$i];
                    }
                }
                return $file_ary;
            }
            return [];

        }

        public static function sendRequest($url,$fields,$requestType='GET',$header=[],$files=false){

            $requestType=strtolower($requestType);
            if(in_array($requestType,['get','post','delete'])){

                //extract data from the post
                //set POST variables
                $url = 'http://qhadath.com/wp-json/wp/v2/'.$url;
                //url-ify the data for the POST
                $fields_string='';
                if(!$files){
                    foreach($fields as $key=>$value) { $fields_string .= $key.'='.rawurlencode($value).'&'; }
                    //print_r($fields_string);
                    //die();
                    rtrim($fields_string, '&');
                    if(in_array($requestType,['get','delete'])){
                        $url.='?'.$fields_string;
                    }
                }

                //open connection
                $POSTFIELDS=($files)?$fields:$fields_string;
                $ch = curl_init();
                //set the url, number of POST vars, POST data
                curl_setopt($ch,CURLOPT_URL, $url);
                if($requestType=='post'){
                    curl_setopt($ch,CURLOPT_POST, true);
                    curl_setopt($ch,CURLOPT_POSTFIELDS,$POSTFIELDS );
                }elseif($requestType=='delete'){
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                }
                $header[]='authorization: Basic YWRtaW5zOmFkbWlucw==';
                curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                //execute post
                $response = curl_exec($ch);
                if($url=='http://qhadath.com/wp-json/wp/v2/media'){
//echo $response;
//die();
                }
                $err = curl_error($ch);
                curl_close($ch);
                return json_decode($response);
            }

        }
        public static function DeleteCategoryWP($wp_id){
            return self::sendRequest('categories/'.$wp_id,['force'=>true],'DELETE');
        }
        public static function InsertCategoryWP($categoryParameters,$ID=null){
            $data=self::sendRequest('categories',$categoryParameters,'post');
            $wp_id=0;
            if(isset($data->code)&& $data->code=='term_exists'){
                $wp_id=$data->data->term_id;
            }elseif(isset($data->id)){
                $wp_id=$data->id;
            }
            global $config;
            $ID=($ID)?$ID: mysqli_insert_id($config);
            $sql_query = "UPDATE tbl_category SET wp_id = '$wp_id' WHERE cid = '$ID'";
            mysqli_query($config,$sql_query);
            return $wp_id;
        }
        public static function DeleteNewsWP($wp_id,$news_id){
            global $config;
            $sql_query="SELECT * FROM tbl_news_gallery WHERE nid='$news_id'";
            $result=mysqli_query($config,$sql_query);
            while($row = $result->fetch_assoc()){
                if($row['wp_id']>0){
                    self::sendRequest('media/'.$row['wp_id'],['force'=>true],'DELETE');
                }
            }
            return self::sendRequest('news/'.$wp_id,['force'=>true],'DELETE');
        }
        public static function InsertNewsWP($newsParameters,$file=null,$fileName=null,$ID=null,$wp_id=null){
            if(!$wp_id||$wp_id==null){
                $data=self::sendRequest('posts',$newsParameters,'post');
                $wp_id=$data->id;
            }else{
               $data=self::sendRequest('posts/'.$wp_id,$newsParameters,'post');
            }
            global $config;
            $ID=($ID)?$ID: mysqli_insert_id($config);
            $sql_query = "UPDATE tbl_news SET wp_id = '$wp_id' WHERE nid = '$ID'";
            mysqli_query($config,$sql_query);
            if($fileName&&$file){
                /*$media=self::sendRequest('media',[
                    'date'=>date('Y-m-d H:i:s'),
                    'date_gmt'=>gmdate('Y-m-d H:i:s'),
                    'status'=>'publish',
                    'title'=>$newsParameters['title'],
                    'description'=>$newsParameters['content'],
                    'caption'=>$news_image_path.$news_image,
                    'post'=>$wp_id
                ],'post',['Content-Disposition: attachment; filename="'.$news_image.'"']);*/
                $media=self::sendRequest('media',$file,'post',['Content-Disposition: attachment; filename="'.$fileName.'"'],true);
                $data=self::sendRequest('posts/'.$wp_id,['featured_media'=>$media->id],'post');
            }else{
                $sql_query="SELECT * FROM tbl_news WHERE nid=$ID";
                $result=mysqli_query($config,$sql_query);
                $row = mysqli_fetch_assoc($result);
                if(!$row['news_image_wp_id']||$row['news_image_wp_id']==null){
                    $imagePath=explode('public',__DIR__)[0].'upload/'.$row['news_image'];
                    if(file_exists($imagePath)){
                        $file=file_get_contents($imagePath);
                        $media=self::sendRequest('media',$file,'post',['Content-Disposition: attachment; filename="'.$row['news_image'].'"'],true);
                        //die();
                        $data=self::sendRequest('posts/'.$wp_id,['featured_media'=>$media->id],'post');
                        $nid=$row['nid'];
                        $sql_query="UPDATE tbl_news SET news_image_wp_id='$wp_id' WHERE nid='$nid'";
                        mysqli_query($config,$sql_query);
                    }
                }
                $sql_query="SELECT * FROM tbl_news_gallery WHERE nid=$ID";
                $result=mysqli_query($config,$sql_query);
                while($row = mysqli_fetch_assoc($result)){
                    if(!$row['wp_id']||$row['wp_id']==null){
                        $imagePath=explode('public',__DIR__)[0].'upload/'.$row['image_name'];
                        if(file_exists($imagePath)){
                            $file=file_get_contents($imagePath);
                            $media=self::sendRequest('media',$file,'post',['Content-Disposition: attachment; filename="'.$row['image_name'].'"'],true);
                            $data=self::sendRequest('posts/'.$wp_id,['featured_media'=>$media->id],'post');
                            $id=$row['id'];
                            $sql_query="UPDATE tbl_news_gallery SET wp_id='$wp_id' WHERE id='$id'";
                            mysqli_query($config,$sql_query);
                        }
                    }
                }
                return $data;
            }


        }
        public static function slugify($text)
        {
           /* // replace non letter or digits by -
            $text = preg_replace('~[^\pL\d]+~u', '-', $text);

            // transliterate
            $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

            // remove unwanted characters
            $text = preg_replace('~[^-\w]+~', '', $text);

            // trim
            $text = trim($text, '-');

            // remove duplicate -
            $text = preg_replace('~-+~', '-', $text);

            // lowercase
            $text = strtolower($text);

            if (empty($text)) {
                return 'n-a';
            }

            return $text;*/
           return str_replace(' ','-',$text);
        }
            
    }

?>