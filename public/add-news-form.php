<?php 
    include('public/fcm.php');
    require_once("public/thumbnail_images.class.php");
    include_once('functions.php');
 
    if(isset($_POST['submit'])) {
      
        $send_notification=isset($_POST['send_notification'])?1:0;
        $urgent=isset($_POST['urgent'])?1:0;
        $video_id = 'cda11up';

        if($_POST['upload_type'] == 'Upload') {

            $news_image = time().'_'.$_FILES['news_image']['name'];
            $pic2            = $_FILES['news_image']['tmp_name'];
            $tpath2          = 'upload/'.$news_image;
            copy($pic2, $tpath2);

            $video  = time().'_'.$_FILES['video']['name'];
            $pic1   = $_FILES['video']['tmp_name'];
            $tpath1 ='upload/video/'.$video;
            copy($pic1, $tpath1);
            $bytes = $_FILES['video']['size'];

            if ($bytes >= 1073741824) {
                $bytes = number_format($bytes / 1073741824, 2) . ' GB';
            }

            else if ($bytes >= 1048576) {
                $bytes = number_format($bytes / 1048576, 2) . ' MB';
            }

            else if ($bytes >= 1024) {
                $bytes = number_format($bytes / 1024, 2) . ' KB';
            }

            else if ($bytes > 1) {
                $bytes = $bytes . ' bytes';
            }

            else if ($bytes == 1) {
                $bytes = $bytes . ' byte';
            } else {
                $bytes = '0 bytes';
            }


        } else if ($_POST['upload_type'] == 'Url') {

            $video = $_POST['url_source'];

            $news_image = time().'_'.$_FILES['image']['name'];
            $pic2            = $_FILES['image']['tmp_name'];
            $tpath2          = 'upload/'.$news_image;
            copy($pic2, $tpath2);

        } else if ($_POST['upload_type'] == 'Post') {

            $news_image = time().'_'.$_FILES['post_image']['name'];
            $pic2            = $_FILES['post_image']['tmp_name'];
            $tpath2          = 'upload/'.$news_image;
            copy($pic2, $tpath2);

            /**
             * multiple upload
             */
            $imageNames  = array();
            // $imageFiles = functions::reArrayFiles($_FILES['imageoption']);

            if(!empty($imageNames)){
                foreach ($imageFiles as $imageFile) {
                    if ($imageFile['error'] == 0) {
                        $newName = time() . '_' . $imageFile['name'];
                        $img     = $imageFile['tmp_name'];
                        $imgPath = 'upload/' . $newName;
                        copy($img, $imgPath);
    
                        $imageNames[] = $newName;
                    }
                }
            }

           


        } else {
            $video = $_POST['youtube'];
            $news_image = '';

            function youtube_id_from_url($url) {

                $pattern =
                '%^# Match any youtube URL
                (?:https?://)?  # Optional scheme. Either http or https
                (?:www\.)?      # Optional www subdomain
                (?:             # Group host alternatives
                  youtu\.be/    # Either youtu.be,
                | youtube\.com  # or youtube.com
                  (?:           # Group path alternatives
                    /embed/     # Either /embed/
                  | /v/         # or /v/
                  | /watch\?v=  # or /watch\?v=
                  )             # End path alternatives.
                )               # End host alternatives.
                ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
                $%x'
                ;

                $result = preg_match($pattern, $url, $matches);

                if (false !== $result) {
                    return $matches[1];
                }
                return false;

            }

            $video_id = youtube_id_from_url($_POST['youtube']);

        }
        // print($_POST['news_date']);die();
$images[]=$news_image;
// print($_POST['news_date']);die();
        $data = array(

            'cat_id'            => $_POST['cat_id'],
            'urgent'            => $urgent,
            'news_title'        => addslashes($_POST['news_title']),
            'video_url'         => isset($video) ? $video : '',
            'video_id'          => $video_id,
            'news_image'        => json_encode($images),
            'news_date'         => $_POST['news_date'],
            'news_description'  => htmlspecialchars_decode(($_POST['news_description']) ),
            'content_type'      => $_POST['upload_type'],
            'size'              => isset($bytes) ? $bytes : '',
            'add_by'              => $_SESSION['userID'],
            'add_date'              => date('Y-m-d H:i:s'),
            );
        if (isset($imageNames) && count($imageNames) > 0) {
            global $config;
            $allImages = [];
            $allImages[] = $news_image;
            foreach ($imageNames as $imageName) {
                $allImages[] = $imageName;
            }
            $data['news_image'] = json_encode($allImages);

        }
//        ?><!--<pre style="padding-left:500px; padding-top:150px">--><?//= var_dump($allImages); die(''); ?><!--</pre>--><?php

        $qry = Insert('tbl_news', $data);
        
        $nid = mysqli_insert_id($config);
        // print_r($nid);die();
        $tags=$_POST['tags'];
        $tags_arr = explode (",", $tags[0]); 
  
foreach ($tags_arr as $value) {
    $dataTags=array(

            'tag_name'                => $value,
            'new_id'                  => $nid,
            );
    $qryTags = Insert('tbl_tags', $dataTags);
}
//$i++;
        if($send_notification){
            $type=$_POST['type'];
            $pesan = $data['news_title'];
            $id = $nid;
            $link = "";

            if ($data['content_type'] == 'youtube') {
                $image = 'http://img.youtube.com/vi/'.$row['video_id'].'/mqdefault.jpg';
            } else {
                $image = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/upload/'.$data['news_image'];
            }

            $users_sql = "SELECT * FROM tbl_fcm_token";

            $users_result = mysqli_query($connect, $users_sql);
            $arrData = array();
            $i=0;
            $arrData[$i] = array();
            while($user_row = mysqli_fetch_assoc($users_result)) {
                $msg = $pesan;
                $img = $image;
                $id = $id;
                $link = $link;
                $data = array("title" => $msg, "image" => $img, "id" => $id, "link" => $link,'type'=>$type);

                // echo SEND_FCM_NOTIFICATION($user_row['token'], $data);
                // if(is_array($arrData)){
                //     print(count($arrData[$i]));die();
                // }else{
                //     print('no');die();
                // }
                if(count($arrData[$i])<=1000){
                    // array_pus/h($arrData[$i],$user_row['token']);
                    $arrData[$i][] = $user_row['token'];
                }else{
                    $i++;
                }
                
                //  $register_ids[] = $user_row['token'];

            }
            // print(count($arrData));die();
            foreach($arrData as $register_ids){
                echo SEND_FCM_NOTIFICATION($register_ids, $data);    
            }
            
        }
        
        $newsParameters=[ 'date'=>date('Y-m-d H:i:s',strtotime($_POST['news_date'])),
            'date_gmt'=>gmdate('Y-m-d H:i:s',strtotime($_POST['news_date'])),
            'status'=>'publish',
            'title'=>$_POST['news_title'],
            'content'=>$_POST['news_description'],
            'categories'=>$_POST['cat_wp_id']];
        $file=($_POST['upload_type']!='youtube'&&!empty($news_image))?file_get_contents( explode('public',__DIR__)[0].'upload/'.$news_image ):null;
        functions::InsertNewsWP($newsParameters,($_POST['upload_type']!='youtube'&&!empty($news_image))?$file:null,($_POST['upload_type']!='youtube'&&!empty($news_image))?$news_image:null);


        $_SESSION['msg'] = "";
        header( "Location:add-news.php");
        exit;

    }

    $sql_category = "SELECT * FROM tbl_category ORDER BY cid DESC";
    $category_result = mysqli_query($connect, $sql_category);
?>

<script type="text/javascript">

    $(document).ready(function(e) {
// alert("dd");
        $("#upload_type").change(function() {
            var type = $("#upload_type").val();

            if (type == "youtube")  {
                
                $("#video_upload").hide();
                $("#direct_url").hide();
                $("#video_post").hide();
                $("#youtube").show();
            }

            if (type == "Url") {
                
                $("#youtube").hide();
                $("#video_upload").hide();
                $("#video_post").hide();
                $("#direct_url").show();
            }

            if (type == "Upload") {
                
                $("#youtube").hide();
                $("#direct_url").hide();
                $("#video_post").hide();
                $("#video_upload").show();
            }

            if (type == "Post") {
                $("#youtube").hide();
                $("#direct_url").hide();
                $("#video_upload").hide();
                $("#video_post").show();

                // $("#multiple_images").hide();
            }
        });

        $( window ).load(function() {
        var type=$("#upload_type").val();

            if (type == "youtube")  {
                
                $("#video_upload").hide();
                $("#direct_url").hide();
                $("#video_post").hide();
                $("#youtube").show();
            }

            if (type == "Url") {
                
                $("#youtube").hide();
                $("#video_upload").hide();
                $("#video_post").hide();
                $("#direct_url").show();
            }

            if (type == "Upload") {
                
                $("#youtube").hide();
                $("#direct_url").hide();
                $("#video_post").hide();
                $("#video_upload").show();
            }

            if (type == "Post") {
                $("#youtube").hide();
                $("#direct_url").hide();
                $("#video_upload").hide();
                $("#video_post").show();

                // $("#multiple_images").hide();
            }
            $("#cat_wp_id").val($("#cat_id option:selected").attr('data-id'));
            $(document).on('change','#cat_id',function(){
                $("#cat_wp_id").val($("#cat_id option:selected").attr('data-id'));
            });
        });

    });

</script>

   <section class="content">
   
        <ol class="breadcrumb">
            <li><a href="dashboard.php">Dashboard</a></li>
            <li><a href="manage-news.php">Manage News</a></li>
            <li class="active">Add News</a></li>
        </ol>

       <div class="container-fluid">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                	<form id="form_validation" method="post" enctype="multipart/form-data">
                    <div class="card">
                        <div class="header">
                            <h2>ADD NEWS</h2>
                                <?php if(isset($_SESSION['msg'])) { ?>
                                    <br><div class='alert alert-info'>New News Added Successfully...</div>
                                    <?php unset($_SESSION['msg']); } ?>
                        </div>
                        <div class="body">

                        	<div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <input type="checkbox" name="send_notification" id="send_notification" value="1">
                                            <label for="send_notification">
                                                Send Notification
                                            </label>
                                        </div>
                                    </div>
                                    <div id="notificationTypes" class="hidden">
                                        <div class="text-center col-sm-4 col-lg-4">
                                            <div class="radio">
                                                <input type="radio" name="type" id="default_type" value="default" checked="checked">
                                                <label for="default_type">
                                                    Send Default Notification
                                                </label>
                                            </div>
                                        </div>
                                        <div class="text-center col-sm-4 col-lg-4">
                                            <div class="radio">
                                                <input type="radio" name="type" id="urgent_type" value="urgent">
                                                <label for="urgent_type">
                                                    Send Urgent Notification
                                                </label>
                                            </div>
                                        </div>
                                        <div class="text-center col-sm-4 col-lg-4">
                                            <div class="radio">
                                                <input type="radio" name="type" id="goal_type" value="goal">
                                                <label for="goal_type">
                                                    Send Goal Notification
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <input type="checkbox" name="urgent" id="urgent" value="1">
                                            <label for="urgent">
                                               Urgent
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">

                                    <div class="form-group">
                                        <div class="font-12">News Title *</div>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="news_title" id="news_title" value="" placeholder="News Title" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="font-12">News Date *</div>
                                        <div class="form-line">
                                            <input type="text" name="news_date" id="date-format" class="datetimepicker form-control" placeholder="News Date" required>
                                        </div>
                                    </div>
                                    <input type="hidden" name="cat_wp_id" id="cat_wp_id">
                                    <div class="form-group">
                                        <div class="font-12">Category *</div>
                                        <select class="form-control show-tick" name="cat_id" id="cat_id">
                                            <?php while ($data = mysqli_fetch_array ($category_result)) { ?>
                                            <option data-id="<?=$data['wp_id']?>" value="<?php echo $data['cid'];?>"><?php echo $data['category_name'];?></option>
                                                <?php } ?>
                                        </select>
                                    </div>



                                
                                    <div class="form-group">
                                        <div class="font-12">Content Type *</div>
                                        <select class="form-control show-tick" name="upload_type" id="upload_type">
                                                <option value="Post">Standard Post</option>
                                                <option value="youtube">Video Post (YouTube)</option>
                                                <option value="Url">Video Post (Url)</option>
                                                <option value="Upload">Video Post (Upload)</option>
                                        </select>
                                    </div>

                                    <div id="video_post">
                                        <div class="font-12 ex1">Image Primary ( jpg / png ) *</div>
                                        <div class="form-group">
                                            <input type="file" name="post_image" id="post_image" class="dropify-image"   data-max-file-size="1M" data-allowed-file-extensions="jpg jpeg png gif" required />
                                         </div>
                                         <div class="font-12">Tags</div>
                                         <input id="form-tags-1" name="tags[]" type="text" value="">
                                        <script type="text/javascript">
                                            $(function() {
                                                $('#form-tags-1').tagsInput();
                                            });
                                        </script>
                                        <div id="multiple_images">
                                            <!-- <div class="font-12 ex1">Image Optional ( jpg / png )</div> -->
                                            <div class="form-group">
                                                <input type="hidden" name="imageoption[]" id="imageoptions"/>
                                            </div>
                                            <div class="multiupload"></div>
                                            <button type="button" class="btn btn-primary waves-effect" id="addnewUpload" >Add more</button>
                                        </div>

                                    </div>

                                    <div id="youtube">
                                        <div class="form-group">
                                            <div class="font-12">Youtube URL</div>
                                            <div class="form-line">
                                                <input type="url" class="form-control" name="youtube" id="youtube" placeholder="https://www.youtube.com/watch?v=33F5DJw3aiU" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="direct_url">
                                        <div class="form-group">
                                            <input type="file" name="image" id="image" class="dropify-image" data-max-file-size="1M" data-allowed-file-extensions="jpg png gif" />
                                        </div>
                                        <div class="form-group">
                                            <div class="font-12">Video URL</div>
                                            <div class="form-line">
                                                <input type="url" class="form-control" name="url_source" id="url_source" placeholder="http://www.xyz.com/news_title.mp4" required/>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="video_upload">
                                        <div class="form-group">
                                            <input type="file" id="news_image" name="news_image" id="news_image" class="dropify-image" data-max-file-size="1M" data-allowed-file-extensions="jpg png gif" required />
                                        </div>

                                        <div class="form-group">
                                            <input type="file" id="video" name="video" id="video" class="dropify-video" data-allowed-file-extensions="3gp mp4 mpg wmv mkv m4v mov flv" required/>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-7">
                                    <div class="font-12">Description *</div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="news_description" id="news_description" class="form-control" cols="60" rows="10" required></textarea>

                                        <?php if ($ENABLE_RTL_MODE == 'true') { ?>
                                        <script>                             
                                            CKEDITOR.replace( 'news_description' );
                                            CKEDITOR.config.contentsLangDirection = 'rtl';

                                        </script>
                                        <?php } else { ?>
                                        <script>                             
                                            CKEDITOR.replace( 'news_description' );
                                        </script>
                                        <?php } ?>
                                    </div>

                                    <button type="submit" name="submit" class="btn bg-blue waves-effect pull-right">PUBLISH</button>
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
            
        </div>

    </section>