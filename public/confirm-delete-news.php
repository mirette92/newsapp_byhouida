<?php include_once('functions.php'); ?>
<?php

	if (isset($_GET['id'])) {
		$ID = $_GET['id'];
	} else {
		$ID = "";
	}
		
	// get image file from menu table
	$sql_query = "SELECT news_image FROM tbl_news WHERE nid = ?";
			
	$stmt = $connect->stmt_init();
	if ($stmt->prepare($sql_query)) {	
		// Bind your variables to replace the ?s
		$stmt->bind_param('s', $ID);
		// Execute query
		$stmt->execute();
		// store result 
		$stmt->store_result();
		$stmt->bind_result($news_image);
		$stmt->fetch();
		$stmt->close();
	}
			
	// delete image file from directory
	$delete = unlink('upload/'."$news_image");

	// get image file from menu table
	$sql_query = "SELECT video_url,wp_id,nid FROM tbl_news WHERE nid = ?";
    $wp_id=0;
    $nid=0;
	$stmt = $connect->stmt_init();
	if ($stmt->prepare($sql_query)) {	
		// Bind your variables to replace the ?s
		$stmt->bind_param('s', $ID);
		// Execute query
		$stmt->execute();
		// store result 
		$stmt->store_result();
		$stmt->bind_result($video_url,$wp_id,$nid);
		$stmt->fetch();
		$stmt->close();
	}
			
	// delete image file from directory
	$delete = unlink('upload/video/'."$video_url");
			
	// delete data from menu table
	$sql_query = "DELETE FROM tbl_news WHERE nid = ?";
			
	$stmt = $connect->stmt_init();
	if ($stmt->prepare($sql_query)) {	
		// Bind your variables to replace the ?s
		$stmt->bind_param('s', $ID);
		// Execute query
		$stmt->execute();
		// store result 
		$delete_result = $stmt->store_result();
		$stmt->close();
        if($wp_id&&$nid){
            functions::DeleteNewsWP($wp_id,$nid);
        }
	}
	// if delete data success back to reservation page
	if($delete_result) {
		header("location: manage-news.php");
	}

?>